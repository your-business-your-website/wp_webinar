jQuery(document).ready(function($) {
	
	// calls appendo
	$('#tester_wrap').appendo({
		allowDelete: true,
		labelAdd: 'Add New Time',
		subSelect: 'li.tester:last'
	});
	
		// slide delete button
	$('#tester_wrap li.tester .remove_slide').live('click', function() {
		if($('#tester_wrap li.tester').size() == 1) {
			alert('Sorry, you need at least one slide');	
		}
		else {
			$(this).parent().slideUp(300, function() {
				$(this).remove();	
			})	
		}
		return false;
	});
	
});