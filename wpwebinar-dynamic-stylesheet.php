<?php

function wpoptin_dynamic_stylesheet() {
global $post;
$post_id = $post->ID; ?>

	<style type="text/css">
		webinar_wrapper {
			padding-top:<?php echo get_post_meta($post_id,'wpoptin_top_margin',true); ?>; 
			width: <?php echo get_post_meta($post_id,'wpoptin_optinpage_width',true); ?>;
		}
	</style>

<?php }

add_action('wp_head','wpoptin_dynamic_stylesheet');

?>