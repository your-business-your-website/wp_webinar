<?php /*** WPWEBINAR WEBINAR TEMPLATE ***/ ?>


<?php 
if ( ! function_exists( 'is_ssl' ) ) {
	function is_ssl() {
		if ( isset($_SERVER['HTTPS']) ) {
			if ( 'on' == strtolower($_SERVER['HTTPS']) )
			return true;
			if ( '1' == $_SERVER['HTTPS'] )
			return true;
		} 
		elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
			return true;
		}
		return false;
	}
}

if ( version_compare( get_bloginfo( 'version' ) , '3.0' , '<' ) && is_ssl() ) {
	$wp_content_url = str_replace( 'http://' , 'https://' , get_option( 'siteurl' ) );
} else {
	$wp_content_url = get_option( 'siteurl' );
}
$wp_content_url .= '/wp-content';
$wp_content_dir = ABSPATH . 'wp-content';
$wp_plugin_url = $wp_content_url . '/plugins';
$wp_plugin_dir = $wp_content_dir . '/plugins';
$wpmu_plugin_url = $wp_content_url . '/mu-plugins';
$wpmu_plugin_dir = $wp_content_dir . '/mu-plugins';

?>

<?php 
global $post;
$post_id = $post->ID;
$yourtheme = get_post_meta($post_id,'wpwebinar_theme_yours',true);
$basictheme = get_post_meta($post_id,'wpwebinar_theme_basic',true);
$moderntheme = get_post_meta($post_id,'wpwebinar_theme_modern',true);
$themecolor = get_post_meta($post_id,'wpwebinar_theme_color',true);

	if ($yourtheme == 'on') {
		get_header(); 
	}
	if ($basictheme == 'on') { ?>
		<html>
		<head>
		<?php wp_head(); ?>
		<link rel='stylesheet' id='wpwebinarstylesheet-css'  href='<?php echo $wp_plugin_url; echo "/wp-webinar/templates/basic/"; ?>style.css' type='text/css' media='all' />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type='text/javascript' src='<?php echo $wp_plugin_url; echo "/wp-webinar/js/"; ?>countdown.php?ver=3.6-beta4-24596'></script>
		<script type='text/javascript' src='<?php echo $wp_plugin_url; echo "/wp-webinar/"; ?>wpwebinar-dynamic-stylesheet.php'></script>

		<?php require_once( plugin_dir_path( __FILE__ ). '/wpwebinar-dynamic-stylesheet.php' ); ?>

		</head>

		<header style="background-color: <?php echo $themecolor; ?>;">
		<div id="headinner">
		<div id="webinarlogo"><img src="<?php
		global $post;
		$post_id = $post->ID;
		$image = get_post_meta($post_id,'wpwebinar_logo',true);
		$image = do_shortcode($image);
		$image = wp_get_attachment_image_src($image, 'full');	$image = $image[0];
			if (empty($image)) { 
				echo ''; } 
			else { 
				echo $image;  
			}
		?>"/></div>
		</div>
		<div style="clear: both;"></div>
		</header>
		<body>
		<div id="pageshadow">

<?php 	} 

	if ($moderntheme == 'on') { ?>
		<html>
		<link rel='stylesheet' id='wpwebinarstylesheet-css'  href='<?php echo $wp_plugin_url; echo "/wp-webinar/templates/modern/"; ?>style.css' type='text/css' media='all' />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type='text/javascript' src='<?php echo $wp_plugin_url; echo "/wp-webinar/js/"; ?>countdown.php?ver=3.6-beta4-24596'></script>

		<header style="background-color: <?php echo $themecolor; ?>;">
		</header>
		<body style="background-color: <?php echo $themecolor; ?>;">
            <div id="bodycolor">
		<?php echo '<center>'; echo wpautop(get_post_meta($post_id,'wpwebinar_main_headline',true)); echo '</center>'; ?>

<?php 	} ?>

<?php 
global $post; 
$post_id = $post->ID; 

	echo '<script type="text/javascript" src="'; 
	echo $wp_plugin_url; echo "/wp-webinar/js/countdown.php?timezone="; 
	if(get_post_meta($post_id,'wpwebinar_admintime',true) == 'on')
		echo get_post_meta($post_id,'wpwebinar_webinar_timezone',true); 
		echo '"></script>'; 
?>

<!-- Custom Styling2 --> 
<style type="text/css">
.optinpagebutton { background-image:url('<?php
global $post;
$post_id = $post->ID;

$button_orange = get_post_meta($post_id,'wpwebinar_button_orange',true);
$button_green = get_post_meta($post_id,'wpwebinar_button_green',true);
$button_blue = get_post_meta($post_id,'wpwebinar_button_blue',true);
	if ($button_orange == 'on') { echo $wp_plugin_url; echo "/wp-webinar/images/orangebutton.png"; }
		else { echo "";}
	if ($button_green == 'on') { echo $wp_plugin_url; echo "/wp-webinar/images/greenbutton.png"; }
		else { echo ""; }
	if ($button_blue == 'on') { echo $wp_plugin_url; echo "/wp-webinar/images/bluebutton.png"; }
		else { echo "";}
?>')!important;}
</style>

<?php 
$values = $_GET["webinar"];
if($values == ""){ ?>

<div id="webinar_wrapper"> <!-- START WRAPPER OPTIN -->
	<!-- ****************************************************************** -->
	<!-- HEADLINE -->
	<!-- ****************************************************************** -->
	
	<?php 
	global $post; 
	$post_id = $post->ID; 
	$yourtheme = get_post_meta($post_id,'wpwebinar_theme_yours',true);
	$basictheme = get_post_meta($post_id,'wpwebinar_theme_basic',true);
	$webinarwidth = get_post_meta($post_id,'wpwebinar_video_width',true);
	$webinarheight = get_post_meta($post_id,'wpwebinar_video_height',true);

	if ($yourtheme == 'on') {	
		echo '<div id="webinar_headline">';
		echo wpautop(get_post_meta($post_id,'wpwebinar_main_headline',true)); 
		echo '</div>';
	}
	if 	($basictheme == 'on') { 
		echo '<div id="webinar_headline">';
		echo wpautop(get_post_meta($post_id,'wpwebinar_main_headline',true)); 
		echo '</div>';
		?>
		<img <?php $basictheme = get_post_meta($post_id,'wpwebinar_theme_basic',true); if 	($basictheme == 'on') { echo 'style="margin-bottom: 24px;"';}?> src="<?php echo $wp_plugin_url; echo "/wp-webinar/templates/basic/images/reg1.png"; ?>" width="980px" height="47px">

	<?php } ?>

	<div id="webinar_content">
		<!-- <div id="webinar_video"> -->

			<link href="https://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
			<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

				<video autoplay id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" max-width="<?php echo $webinarwidth; ?>" height="
				<?php echo $webinarheight; ?>" data-setup="{}" >
				<source src="<?php echo get_post_meta($post_id,'wpwebinar_video_url',true); ?>" type='video/mp4'>
				</video>

			<script src="https://vjs.zencdn.net/6.6.3/video.js"></script>

		<!--</div><!-- END WEBINAR VIDEO -->

		<!-- ****************************************************************** -->
		<!-- TEXT BELOW VIDEO -->
		<!-- ****************************************************************** -->

		<?php
		global $post;
		$post_id = $post->ID;
			echo wpautop(get_post_meta($post_id,'wpwebinar_optional_text',true));
		?>

	</div><!-- END WEBINARCONTENT -->
	
	<!-- ****************************************************************** -->
	<!-- WEBINAR SIDEBAR -->
	<!-- ****************************************************************** -->

	<div id="webinarsidebar" <?php $themecolor = get_post_meta($post_id,'wpwebinar_theme_color',true); $moderntheme = get_post_meta($post_id,'wpwebinar_theme_modern',true); if (($moderntheme == 'on')) { 
            echo 'style="background-color:';  
            echo $themecolor;
            echo ';';
            echo 'background-image:url(\'';
            echo $wp_plugin_url; 
            echo "/wp-webinar/templates/modern/images/transback.png";
            echo '\');"';
        } 
    else { 
        echo '';
    } ?>>
	<!-- START WEBINAR SIDEBAR -->

		<!-- ****************************************************************** -->
		<!-- WEBINAR BOX HEADLINE -->
		<!-- ****************************************************************** -->
		<div id="sidebarhead" style="background-color: <?php $themecolor = get_post_meta($post_id,'wpwebinar_theme_color',true); $moderntheme = get_post_meta($post_id,'wpwebinar_theme_modern',true); if (($moderntheme == 'on') || ($yourtheme == 'on')) { echo 'transparent';} else { echo $themecolor;} ?>;">
		<h2><?php global $post; $post_id = $post->ID; echo get_post_meta($post_id,'wpwebinar_box_headline',true); ?></h2>
		</div>
		<?php  
	    $moderntheme = get_post_meta($post_id,'wpwebinar_theme_modern',true); 
	    $yourtheme = get_post_meta($post_id,'wpwebinar_theme_yours',true);
		if (($moderntheme == 'on') || ($yourtheme == 'on')) { 
			echo '';
		} 
		else { 
		    echo '<div style="width: 0; height: 0; border-left: 20px solid transparent; border-right: 20px solid transparent; margin: 0px auto; border-top: 20px solid <?php $themecolor = get_post_meta($post_id,\'wpwebinar_theme_color\',true); echo $themecolor;} ?>;"></div>';
		} ?>
		<div id="sidebarcontent">
		<!-- ****************************************************************** -->
		<!-- WEBINAR BOX DECRIPTION -->
		<!-- ****************************************************************** -->
		<?php global $post; $post_id = $post->ID; echo wpautop(get_post_meta($post_id,'wpwebinar_box_description',true)); ?>
</div>
		<!-- ****************************************************************** -->
		<!-- DATE AND TIME DROP DOWN BOXES -->
		<!-- ****************************************************************** -->

		<script type="text/javascript">
			function copyEmail(){
				var f1 = document.getElementById("email");
				var f2 = document.getElementById("autoemail");
				f2.value = f1.value;
			}
			function copyName(){
				var f3 = document.getElementById("first_name");
				var f4 = document.getElementById("autoname");
				f4.value = f3.value;
			}
		</script>	

<script type="text/javascript">
jQuery(document).ready(function($) {
    $("#form1").submit(function(e) {
        var date = $("#date").val();
        if (date == "-1") {
			e.preventDefault();
            alert("Please Select Webinar Date");
        } else {
			var time = $("#time").val();
			if (time == "-1") {
				e.preventDefault();
				alert("Please Select Webinar Time");
			} else {
				var name = $("#first_name").val();
				if (name == "Enter Your First Name") {
					e.preventDefault();
					alert("Please Enter Your First Name");
				} else {
					var email = $("#email").val();
					if (email == "Type In Your Email Address") {
						e.preventDefault();
						alert("Please Type In Your Email Address");
					} 
				}
			}
        }				
    });

	$(function(){
		$('input.optinpagebutton').click(function(event){
			$.post($("#form2").attr("action"), $("#form2").serialize(),
				function(data) {
				$("#msg").append(data);
			});		
		});
	});

	$(function(){
		$('input.optinpagebutton').click(function(event){
			event.preventDefault();
			var button = this;
			window.setTimeout(function(form){
				$(form).submit();
			}, 500, $(button).closest('form'));
		});
	});

});
</script>

		<?php 
			global $post;
			$post_id = $post->ID;
			$aweber = get_post_meta($post_id,'wpwebinar_aweber',true);
			$oneshoppingcart = get_post_meta($post_id,'wpwebinar_oneshoppingcart',true);
			$arp3 = get_post_meta($post_id,'wpwebinar_arp3',true);
			$getresponse = get_post_meta($post_id,'wpwebinar_getresponse',true);
			$other = get_post_meta($post_id,'wpwebinar_other',true);
			$page = get_permalink( $_REQUEST['posted'] );
			
			//Aweber Form Set
			if ( (get_post_meta($post_id,'wpwebinar_recurring_webinar',true) == 'on') ) { 
					echo '<form name="form1" id="form1" action="">';
					echo '<input type="hidden" name="webinar" id="webinar" value="processing1" />';
					echo '<input type="hidden" name="type" id="type" value="r" /> ';
			} 
			if ( (get_post_meta($post_id,'wpwebinar_onetime_webinar',true) == 'on') ) {
					echo '<form name="form1" id="form1" action="">'. "\n"; 
					echo '<input type="hidden" name="webinar" id="webinar" value="processing1" />'. "\n"; 
					echo '<input type="hidden" name="type" id="type" value="o" /> '. "\n"; 
					echo '<input type="hidden" name="date" id="date" value="';
					echo get_post_meta($post_id,'wpwebinar_webinar_year',true);
					echo '-';
					echo get_post_meta($post_id,'wpwebinar_webinar_month',true);
					echo '-';
					echo get_post_meta($post_id,'wpwebinar_webinar_day',true);
					echo '" />'. "\n"; 
					echo '<input type="hidden" name="time" id="time" value="';
					echo get_post_meta($post_id,'wpwebinar_webinar_hour',true);
					echo '" />'. "\n"; 
			} 
	
		?>
		
		<!-- RECURRING WEBINAR ON -->
		
		<?php if(get_post_meta($post_id,'wpwebinar_recurring_webinar',true) == "on"){ ?>

<script type='text/javascript'>//<![CDATA[ 
jQuery(document).ready(function($) {
        $("#date").html($('#date option').sort(function(x, y) {
            return $(x).val() < $(y).val() ? -1 : 1;
        }))
        $("#date").get(0).selectedIndex = 0;
});//]]>  
</script>

<script type='text/javascript'>
window.onload = function(){
	selectCount = document.getElementById("date").length;
	if (selectCount == "2") {
		document.getElementById("date").options.length=2;
	}
	if (selectCount == "3") {
		document.getElementById("date").options.length=3;
	}
	if (selectCount >= "4") {
		document.getElementById("date").options.length=4;
	}
}
</script>

			<div style="text-align: center;">
				<?php 
				global $post;
				$post_id = $post->ID;
					$webinarsunday = get_post_meta($post_id,'wpwebinar_sunday',true);
					$webinarmonday = get_post_meta($post_id,'wpwebinar_monday',true);
					$webinartuesday = get_post_meta($post_id,'wpwebinar_tuesday',true);
					$webinarwednesday = get_post_meta($post_id,'wpwebinar_wednesday',true);
					$webinarthursday = get_post_meta($post_id,'wpwebinar_thursday',true);
					$webinarfriday = get_post_meta($post_id,'wpwebinar_friday',true);
					$webinarsaturday = get_post_meta($post_id,'wpwebinar_saturday',true);
					
					$daycount = 0;
					if($webinarsunday) {
						$daycount++;
					}
					if($webinarmonday) {
						$daycount++;
					}
					if($webinartuesday) {
						$daycount++;
					}
					if($webinarwednesday) {
						$daycount++;
					}
					if($webinarthursday) {
						$daycount++;
					}
					if($webinarfriday) {
						$daycount++;
					}
					if($webinarsaturday) {
						$daycount++;
					}
					// echo $daycount;
				?>


			<?php if($daycount > 1) { ?>

				<?php if ($moderntheme == 'on') { 
					echo '<div class="optintext"><b>1.)</b> Which Day Do You Want To Attend?</div>';
					} ?>
				<select id ="date" name="date">
					<option value="-1">Select The Webinar Date</option>
					
					<?php if ($webinarsunday == 'on' && ((date(N) == "4") || (date(N) == "5") || (date(N) == "6"))) { ?><option value="<?php $sundaytime = strtotime("this Sunday "); echo date('Y-m-d',$sundaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Sunday")); ?></option> <?php } ?>
					<?php if ($webinarsunday == 'on' && ((date(N) == "7") || (date(N) == "1") || (date(N) == "2") || (date(N) == "3"))) { ?><option value="<?php $sundaytime = strtotime("next Sunday "); echo date('Y-m-d',$sundaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Sunday")); ?></option><?php } ?>
					
					<?php if ($webinarmonday == 'on' && ((date(N) == "5") || (date(N) == "6") || (date(N) == "7"))) { ?><option value="<?php $mondaytime = strtotime("this Monday"); echo date('Y-m-d',$mondaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Monday")); ?></option> <?php } ?>
					<?php if ($webinarmonday == 'on' && ((date(N) == "1") || (date(N) == "2") || (date(N) == "3") || (date(N) == "4"))) { ?><option value="<?php $mondaytime = strtotime("next Monday"); echo date('Y-m-d',$mondaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Monday")); ?></option> <?php } ?>

					<?php if ($webinartuesday == 'on' && ((date(N) == "6") || (date(N) == "7") || (date(N) == "1"))) { ?><option value="<?php $tuesdaytime = strtotime("this Tuesday"); echo date('Y-m-d',$tuesdaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Tuesday")); ?></option> <?php } ?>
					<?php if ($webinartuesday == 'on' && ((date(N) == "2") || (date(N) == "3") || (date(N) == "4") || (date(N) == "5"))) { ?><option value="<?php $tuesdaytime = strtotime("next Tuesday"); echo date('Y-m-d',$tuesdaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Tuesday")); ?></option> <?php } ?>

					<?php if ($webinarwednesday == 'on' && ((date(N) == "7") || (date(N) == "1") || (date(N) == "2"))) { ?><option value="<?php $wednesdaytime = strtotime("this Wednesday"); echo date('Y-m-d',$wednesdaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Wednesday")); ?></option> <?php } ?>
					<?php if ($webinarwednesday == 'on' && ((date(N) == "3") || (date(N) == "4") || (date(N) == "5") || (date(N) == "6"))) { ?><option value="<?php $wednesdaytime = strtotime("next Wednesday"); echo date('Y-m-d',$wednesdaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Wednesday")); ?></option> <?php } ?>

					<?php if ($webinarthursday == 'on' && ((date(N) == "1") || (date(N) == "2") || (date(N) == "3"))) { ?><option value="<?php $thursdaytime = strtotime("this Thursday"); echo date('Y-m-d',$thursdaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Thursday")); ?></option><?php } ?>
					<?php if ($webinarthursday == 'on' && ((date(N) == "4") || (date(N) == "5") || (date(N) == "6") || (date(N) == "7"))) { ?><option value="<?php $thursdaytime = strtotime("next Thursday"); echo date('Y-m-d',$thursdaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Thursday")); ?></option><?php } ?>

					<?php if ($webinarfriday == 'on' && ((date(N) == "2") || (date(N) == "3") || (date(N) == "4"))) { ?><option value="<?php $fridaytime = strtotime("this Friday"); echo date('Y-m-d',$fridaytime); ?>"> <?php echo date("l F jS, Y", strtotime("this Friday")); ?></option> <?php } ?>
					<?php if ($webinarfriday == 'on' && ((date(N) == "5") || (date(N) == "6") || (date(N) == "7") || (date(N) == "1"))) { ?><option value="<?php $fridaytime = strtotime("next Friday"); echo date('Y-m-d',$fridaytime); ?>"> <?php echo date("l F jS, Y", strtotime("next Friday")); ?></option> <?php } ?>

					<?php if ($webinarsaturday == 'on' && ((date(N) == "3") || (date(N) == "4") || (date(N) == "5"))) { ?><option value="<?php $saturdaytime = strtotime("this Saturday"); echo date('Y-m-d',$saturdaytime); ?>"><?php echo date("l F jS, Y", strtotime("this Saturday")); ?></option><?php } ?>
					<?php if ($webinarsaturday == 'on' && ((date(N) == "6") || (date(N) == "7") || (date(N) == "1") || (date(N) == "2"))) { ?><option value="<?php $saturdaytime = strtotime("next Saturday"); echo date('Y-m-d',$saturdaytime); ?>"><?php echo date("l F jS, Y", strtotime("next Saturday")); ?></option><?php } ?>
				</select>

			<?php }
			else {
				if($webinarsunday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $sundaytime = strtotime("this Sunday"); echo date('Y-m-d',$sundaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Sunday")); ?></div></input>
				<?php }
				if($webinarmonday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $mondaytime = strtotime("this Monday"); echo date('Y-m-d',$mondaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Monday")); ?></div></input>
				<?php }
				if($webinartuesday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $tuesdaytime = strtotime("this Tuesday"); echo date('Y-m-d',$tuesdaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Tuesday")); ?></div></input>
				<?php }
				if($webinarwednesday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $wednesdaytime = strtotime("this Wednesday"); echo date('Y-m-d',$wednesdaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Wednesday")); ?></div></input>
				<?php }
				if($webinarthursday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $thursdaytime = strtotime("this Thursday"); echo date('Y-m-d',$thursdaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Thursday")); ?></div></input>
				<?php }
				if($webinarfriday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $fridaytime = strtotime("this Friday"); echo date('Y-m-d',$fridaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Friday")); ?></div></input>
				<?php }
				if($webinarsaturday == "on") { ?>
					<input type="hidden" id="date" name="date" value="<?php $saturdaytime = strtotime("this Saturday"); echo date('Y-m-d',$saturdaytime); ?>"><div id="timedate1"><strong>Date:</strong> <?php echo date("l F jS, Y", strtotime("this Saturday")); ?></div></input>
				<?php }
			}
			?>

			<!-- ****************************************************************** -->
			<!-- SORTS DROP DOWN-->
			<!-- ****************************************************************** -->

       			<script type="text/javascript">
					jQuery(document).ready(function($) {
					var $dd = $('#date');
					if ($dd.length > 0) { 
						var selectedVal = $dd.val();
						var $options = $('option', $dd);
						var arrVals = [];
						$options.each(function(){
							arrVals.push({
								val: $(this).val(),
								text: $(this).text()
							});
						});
						arrVals.sort(function(a, b){
							return a.val - b.val;
						});
						for (var i = 0, l = arrVals.length; i < l; i++) {
							$($options[i]).val(arrVals[i].val).text(arrVals[i].text);
						}
						$dd.val(selectedVal);
					}
					});
				</script>
				
			</div>
			<div style="clear:all;"></div>

			<!-- ****************************************************************** -->
			<!-- DYNAMIC TIME -->
			<!-- ****************************************************************** -->
				<?php if ($moderntheme == 'on') { 
					echo '<div class="optintext"><b>2.)</b> Which Time Is Best For You?</div>';
					} ?>
				<?php if($daycount > 1) { ?>
					<div id="webinar_local_time">Your Local Time Is: <script type="text/javascript">
					<!--
						var currentTime = new Date()
						var hours = currentTime.getHours()
						var minutes = currentTime.getMinutes()

						if (minutes < 10)
						minutes = "0" + minutes

						var suffix = "AM";
						if (hours >= 12) {
						suffix = "PM";
						hours = hours - 12;
						}
						if (hours == 0) {
						hours = 12;
						}

						document.write("<b>" + hours + ":" + minutes + " " + suffix + "</b>")
					//-->
					</script></div>

			<div style="clear: both;"></div>
			<?php }
			else {

			}
			?>

		<?php 
			global $post;
			$post_id = $post->ID;
			$recurring_webinar_times = get_post_meta($post_id,'wpwebinar_recurring_time',true);
			if($daycount > 1) { ?>


			<select id="time" name="time">
			<option value="-1">Select The Webinar Time</option>
			<?php
				global $post;
				$post_id = $post->ID;
				$recurring_webinar_times = get_post_meta($post_id,'wpwebinar_recurring_time',true);
				$admin_time = get_post_meta($post_id,'wpwebinar_admintime',true);
				$local_time = get_post_meta($post_id,'wpwebinar_localtime',true);
				$gmt_time_zone = get_post_meta($post_id,'wpwebinar_webinar_timezone',true);

				if ($local_time == 'on') { // IF LOCAL TIME
					if ($recurring_webinar_times !='' || $recurring_webinar_time !='') {
						foreach ($recurring_webinar_times as $recurring_webinar_time) { ?>
						<option value="<?php echo $recurring_webinar_time; ?>">
							<?php 	if ($recurring_webinar_time == '1') { echo '1:00 AM Local Time'; } 
									if ($recurring_webinar_time == '2') { echo '2:00 AM Local Time'; } 
									if ($recurring_webinar_time == '3') { echo '3:00 AM Local Time'; } 
									if ($recurring_webinar_time == '4') { echo '4:00 AM Local Time'; }
									if ($recurring_webinar_time == '5') { echo '5:00 AM Local Time'; } 
									if ($recurring_webinar_time == '6') { echo '6:00 AM Local Time'; } 
									if ($recurring_webinar_time == '7') { echo '7:00 AM Local Time'; } 
									if ($recurring_webinar_time == '8') { echo '8:00 AM Local Time'; } 
									if ($recurring_webinar_time == '9') { echo '9:00 AM Local Time'; } 
									if ($recurring_webinar_time == '10') { echo '10:00 AM Local Time'; } 
									if ($recurring_webinar_time == '11') { echo '11:00 AM Local Time'; } 
									if ($recurring_webinar_time == '12') { echo '12:00 PM Local Time'; } 
									if ($recurring_webinar_time == '13') { echo '1:00 PM Local Time'; } 
									if ($recurring_webinar_time == '14') { echo '2:00 PM Local Time'; } 
									if ($recurring_webinar_time == '15') { echo '3:00 PM Local Time'; } 
									if ($recurring_webinar_time == '16') { echo '4:00 PM Local Time'; } 
									if ($recurring_webinar_time == '17') { echo '5:00 PM Local Time'; } 
									if ($recurring_webinar_time == '18') { echo '6:00 PM Local Time'; } 
									if ($recurring_webinar_time == '19') { echo '7:00 PM Local Time'; } 
									if ($recurring_webinar_time == '20') { echo '8:00 PM Local Time'; } 
									if ($recurring_webinar_time == '21') { echo '9:00 PM Local Time'; } 
									if ($recurring_webinar_time == '22') { echo '10:00 PM Local Time'; } 
									if ($recurring_webinar_time == '23') { echo '11:00 PM Local Time'; } 
									if ($recurring_webinar_time == '24') { echo '12:00 AM Local Time'; } 
						?></option>
					<?php } 
					} 
				}
				
				if ($gmt_time_zone == "Pacific/Midway") { $gmt_time_zone_set = '(GMT-11:00) Midway Island, Samoa'; }
				if ($gmt_time_zone == "America/Adak") { $gmt_time_zone_set = '(GMT-10:00) Hawaii-Aleutian'; }
				if ($gmt_time_zone == "Etc/GMT+10") { $gmt_time_zone_set = '(GMT-10:00) Hawaii'; }
				if ($gmt_time_zone == "Pacific/Marquesas") { $gmt_time_zone_set = '(GMT-09:30) Marquesas Islands'; }
				if ($gmt_time_zone == "Pacific/Gambier") { $gmt_time_zone_set = '(GMT-09:00) Gambier Islands'; }
				if ($gmt_time_zone == "America/Anchorage") { $gmt_time_zone_set = '(GMT-09:00) Alaska'; }
				if ($gmt_time_zone == "America/Ensenada") { $gmt_time_zone_set = '(GMT-08:00) Tijuana, Baja California'; }
				if ($gmt_time_zone == "Etc/GMT+8") { $gmt_time_zone_set = '(GMT-08:00) Pitcairn Islands'; }
				if ($gmt_time_zone == "America/Los_Angeles") { $gmt_time_zone_set = '(GMT-08:00) Pacific Time (US & Canada)'; }
				if ($gmt_time_zone == "America/Denver") { $gmt_time_zone_set = '(GMT-07:00) Mountain Time (US & Canada)'; }
				if ($gmt_time_zone == "America/Chihuahua") { $gmt_time_zone_set = '(GMT-07:00) Chihuahua, La Paz, Mazatlan'; }
				if ($gmt_time_zone == "America/Dawson_Creek") { $gmt_time_zone_set = '(GMT-07:00) Arizona'; }
				if ($gmt_time_zone == "America/Belize") { $gmt_time_zone_set = '(GMT-06:00) Saskatchewan, Central America'; }
				if ($gmt_time_zone == "America/Cancun") { $gmt_time_zone_set = '(GMT-06:00) Guadalajara, Mexico City, Monterrey'; }
				if ($gmt_time_zone == "Chile/EasterIsland") { $gmt_time_zone_set = '(GMT-06:00) Easter Island'; }
				if ($gmt_time_zone == "America/Chicago") { $gmt_time_zone_set = '(GMT-06:00) Central Time (US & Canada)'; }
				if ($gmt_time_zone == "America/New_York") { $gmt_time_zone_set = '(GMT-05:00) Eastern Time (US & Canada)'; }
				if ($gmt_time_zone == "America/Havana") { $gmt_time_zone_set = '(GMT-05:00) Cuba'; }
				if ($gmt_time_zone == "America/Bogota") { $gmt_time_zone_set = '(GMT-05:00) Bogota, Lima, Quito, Rio Branco'; }
				if ($gmt_time_zone == "America/Caracas") { $gmt_time_zone_set = '(GMT-04:30) Caracas'; }
				if ($gmt_time_zone == "America/Santiago") { $gmt_time_zone_set = '(GMT-04:00) Santiago'; }
				if ($gmt_time_zone == "America/La_Paz") { $gmt_time_zone_set = '(GMT-04:00) La Paz'; }
				if ($gmt_time_zone == "Atlantic/Stanley") { $gmt_time_zone_set = '(GMT-04:00) Faukland Islands'; }
				if ($gmt_time_zone == "America/Campo_Grande") { $gmt_time_zone_set = '(GMT-04:00) Brazil'; }
				if ($gmt_time_zone == "America/Goose_Bay") { $gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Goose Bay)'; }
				if ($gmt_time_zone == "America/Glace_Bay") { $gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Canada)'; }
				if ($gmt_time_zone == "America/St_Johns") { $gmt_time_zone_set = '(GMT-03:30) Newfoundland'; }
				if ($gmt_time_zone == "America/Araguaina") { $gmt_time_zone_set = '(GMT-03:00) UTC-3'; }
				if ($gmt_time_zone == "America/Montevideo") { $gmt_time_zone_set = '(GMT-03:00) Montevideo'; }
				if ($gmt_time_zone == "America/Miquelon") { $gmt_time_zone_set = '(GMT-03:00) Miquelon, St. Pierre'; }
				if ($gmt_time_zone == "America/Godthab") { $gmt_time_zone_set = '(GMT-03:00) Greenland'; }
				if ($gmt_time_zone == "America/Argentina/Buenos_Aires") { $gmt_time_zone_set = '(GMT-03:00) Buenos Aires'; }
				if ($gmt_time_zone == "America/Sao_Paulo") { $gmt_time_zone_set = '(GMT-03:00) Brasilia'; }
				if ($gmt_time_zone == "America/Noronha") { $gmt_time_zone_set = '(GMT-02:00) Mid-Atlantic'; }
				if ($gmt_time_zone == "Atlantic/Cape_Verde") { $gmt_time_zone_set = '(GMT-01:00) Cape Verde Is.'; }
				if ($gmt_time_zone == "Atlantic/Azores") { $gmt_time_zone_set = '(GMT-01:00) Azores'; }
				if ($gmt_time_zone == "Europe/Belfast") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Belfast'; }
				if ($gmt_time_zone == "Europe/Dublin") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Dublin'; }
				if ($gmt_time_zone == "Europe/Lisbon") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Lisbon'; }
				if ($gmt_time_zone == "Europe/London") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : London'; }
				if ($gmt_time_zone == "Africa/Abidjan") { $gmt_time_zone_set = '(GMT) Monrovia, Reykjavik'; }
				if ($gmt_time_zone == "Europe/Amsterdam") { $gmt_time_zone_set = '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'; }
				if ($gmt_time_zone == "Europe/Belgrade") { $gmt_time_zone_set = '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'; }
				if ($gmt_time_zone == "Europe/Brussels") { $gmt_time_zone_set = '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'; }
				if ($gmt_time_zone == "Africa/Algiers") { $gmt_time_zone_set = '(GMT+01:00) West Central Africa'; }
				if ($gmt_time_zone == "Africa/Windhoek") { $gmt_time_zone_set = '(GMT+01:00) Windhoek'; }
				if ($gmt_time_zone == "Asia/Beirut") { $gmt_time_zone_set = '(GMT+02:00) Beirut'; }
				if ($gmt_time_zone == "Africa/Cairo") { $gmt_time_zone_set = '(GMT+02:00) Cairo'; }
				if ($gmt_time_zone == "Asia/Gaza") { $gmt_time_zone_set = '(GMT+02:00) Gaza'; }
				if ($gmt_time_zone == "Africa/Blantyre") { $gmt_time_zone_set = '(GMT+02:00) Harare, Pretoria'; }
				if ($gmt_time_zone == "Asia/Jerusalem") { $gmt_time_zone_set = '(GMT+02:00) Jerusalem'; }
				if ($gmt_time_zone == "Europe/Minsk") { $gmt_time_zone_set = '(GMT+02:00) Minsk'; }
				if ($gmt_time_zone == "Asia/Damascus") { $gmt_time_zone_set = '(GMT+02:00) Syria'; }
				if ($gmt_time_zone == "Europe/Moscow") { $gmt_time_zone_set = '(GMT+03:00) Moscow, St. Petersburg, Volgograd'; }
				if ($gmt_time_zone == "Africa/Addis_Ababa") { $gmt_time_zone_set = '(GMT+03:00) Nairobi'; }
				if ($gmt_time_zone == "Asia/Tehran") { $gmt_time_zone_set = '(GMT+03:30) Tehran'; }
				if ($gmt_time_zone == "Asia/Dubai") { $gmt_time_zone_set = '(GMT+04:00) Abu Dhabi, Muscat'; }
				if ($gmt_time_zone == "Asia/Yerevan") { $gmt_time_zone_set = '(GMT+04:00) Yerevan'; }
				if ($gmt_time_zone == "Asia/Kabul") { $gmt_time_zone_set = '(GMT+04:30) Kabul'; }
				if ($gmt_time_zone == "Asia/Yekaterinburg") { $gmt_time_zone_set = '(GMT+05:00) Ekaterinburg'; }
				if ($gmt_time_zone == "Asia/Tashkent") { $gmt_time_zone_set = '(GMT+05:00) Tashkent'; }
				if ($gmt_time_zone == "Asia/Kolkata") { $gmt_time_zone_set = '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'; }
				if ($gmt_time_zone == "Asia/Katmandu") { $gmt_time_zone_set = '(GMT+05:45) Kathmandu'; }
				if ($gmt_time_zone == "Asia/Dhaka") { $gmt_time_zone_set = '(GMT+06:00) Astana, Dhaka'; }
				if ($gmt_time_zone == "Asia/Novosibirsk") { $gmt_time_zone_set = '(GMT+06:00) Novosibirsk'; }
				if ($gmt_time_zone == "Asia/Rangoon") { $gmt_time_zone_set = '(GMT+06:30) Yangon (Rangoon)'; }
				if ($gmt_time_zone == "Asia/Bangkok") { $gmt_time_zone_set = '(GMT+07:00) Bangkok, Hanoi, Jakarta'; }
				if ($gmt_time_zone == "Asia/Krasnoyarsk") { $gmt_time_zone_set = '(GMT+07:00) Krasnoyarsk'; }
				if ($gmt_time_zone == "Asia/Hong_Kong") { $gmt_time_zone_set = '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'; }
				if ($gmt_time_zone == "Asia/Irkutsk") { $gmt_time_zone_set = '(GMT+08:00) Irkutsk, Ulaan Bataar'; }
				if ($gmt_time_zone == "Australia/Perth") { $gmt_time_zone_set = '(GMT+08:00) Perth'; }
				if ($gmt_time_zone == "Australia/Eucla") { $gmt_time_zone_set = '(GMT+08:45) Eucla'; }
				if ($gmt_time_zone == "Asia/Tokyo") { $gmt_time_zone_set = '(GMT+09:00) Osaka, Sapporo, Tokyo'; }
				if ($gmt_time_zone == "Asia/Seoul") { $gmt_time_zone_set = '(GMT+09:00) Seoul'; }
				if ($gmt_time_zone == "Asia/Yakutsk") { $gmt_time_zone_set = '(GMT+09:00) Yakutsk'; }
				if ($gmt_time_zone == "Australia/Adelaide") { $gmt_time_zone_set = '(GMT+09:30) Adelaide'; }
				if ($gmt_time_zone == "Australia/Darwin") { $gmt_time_zone_set = '(GMT+09:30) Darwin'; }
				if ($gmt_time_zone == "Australia/Brisbane") { $gmt_time_zone_set = '(GMT+10:00) Brisbane'; }
				if ($gmt_time_zone == "Australia/Hobart") { $gmt_time_zone_set = '(GMT+10:00) Hobart'; }
				if ($gmt_time_zone == "Asia/Vladivostok") { $gmt_time_zone_set = '(GMT+10:00) Vladivostok'; }
				if ($gmt_time_zone == "Australia/Lord_Howe") { $gmt_time_zone_set = '(GMT+10:30) Lord Howe Island'; }
				if ($gmt_time_zone == "Etc/GMT-11") { $gmt_time_zone_set = '(GMT+11:00) Solomon Is., New Caledonia'; }
				if ($gmt_time_zone == "Asia/Magadan") { $gmt_time_zone_set = '(GMT+11:00) Magadan'; }
				if ($gmt_time_zone == "Pacific/Norfolk") { $gmt_time_zone_set = '(GMT+11:30) Norfolk Island'; }
				if ($gmt_time_zone == "Asia/Anadyr") { $gmt_time_zone_set = '(GMT+12:00) Anadyr, Kamchatka'; }
				if ($gmt_time_zone == "Pacific/Auckland") { $gmt_time_zone_set = '(GMT+12:00) Auckland, Wellington'; }
				if ($gmt_time_zone == "Etc/GMT-12") { $gmt_time_zone_set = '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'; }
				if ($gmt_time_zone == "Pacific/Chatham") { $gmt_time_zone_set = '(GMT+12:45) Chatham Islands'; }
				if ($gmt_time_zone == "Pacific/Tongatapu") { $gmt_time_zone_set = '(GMT+13:00) Nuku\'alofa'; }
				if ($gmt_time_zone == "Pacific/Kiritimati") { $gmt_time_zone_set = '(GMT+14:00) Kiritimati'; }
				
				if ($admin_time == 'on') { // IF ADMIN TIME
					if ($recurring_webinar_times !='' || $recurring_webinar_time !='') {
						foreach ($recurring_webinar_times as $recurring_webinar_time) { ?>
						<option value="<?php echo $recurring_webinar_time; ?>">
							<?php 	if ($recurring_webinar_time == '1') { echo '1:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '2') { echo '2:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '3') { echo '3:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '4') { echo '4:00 AM '.$gmt_time_zone_set.''; }
									if ($recurring_webinar_time == '5') { echo '5:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '6') { echo '6:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '7') { echo '7:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '8') { echo '8:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '9') { echo '9:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '10') { echo '10:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '11') { echo '11:00 AM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '12') { echo '12:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '13') { echo '1:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '14') { echo '2:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '15') { echo '3:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '16') { echo '4:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '17') { echo '5:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '18') { echo '6:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '19') { echo '7:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '20') { echo '8:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '21') { echo '9:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '22') { echo '10:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '23') { echo '11:00 PM '.$gmt_time_zone_set.''; } 
									if ($recurring_webinar_time == '24') { echo '12:00 AM '.$gmt_time_zone_set.''; } 
						?></option>
					<?php } 
					} 
				}
				?>
			</select>

			<?php }

				else {
				if ($recurring_webinar_times !='' || $recurring_webinar_time !='') {
						foreach ($recurring_webinar_times as $recurring_webinar_time) { ?>
						<input type="hidden" id="time" name="time" value="<?php echo $recurring_webinar_time; ?>"></input>
							<?php 	if ($recurring_webinar_time == '1') { echo '<div id="timedate"><strong>Time: </strong>1:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '2') { echo '<div id="timedate"><strong>Time: </strong>2:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '3') { echo '<div id="timedate"><strong>Time: </strong>3:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '4') { echo '<div id="timedate"><strong>Time: </strong>4:00 AM Local Time</div>'; }
									if ($recurring_webinar_time == '5') { echo '<div id="timedate"><strong>Time: </strong>5:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '6') { echo '<div id="timedate"><strong>Time: </strong>6:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '7') { echo '<div id="timedate"><strong>Time: </strong>7:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '8') { echo '<div id="timedate"><strong>Time: </strong>8:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '9') { echo '<div id="timedate"><strong>Time: </strong>9:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '10') { echo '<div id="timedate"><strong>Time: </strong>10:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '11') { echo '<div id="timedate"><strong>Time: </strong>11:00 AM Local Time</div>'; } 
									if ($recurring_webinar_time == '12') { echo '<div id="timedate"><strong>Time: </strong>12:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '13') { echo '<div id="timedate"><strong>Time: </strong>1:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '14') { echo '<div id="timedate"><strong>Time: </strong>2:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '15') { echo '<div id="timedate"><strong>Time: </strong>3:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '16') { echo '<div id="timedate"><strong>Time: </strong>4:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '17') { echo '<div id="timedate"><strong>Time: </strong>5:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '18') { echo '<div id="timedate"><strong>Time: </strong>6:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '19') { echo '<div id="timedate"><strong>Time: </strong>7:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '20') { echo '<div id="timedate"><strong>Time: </strong>8:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '21') { echo '<div id="timedate"><strong>Time: </strong>9:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '22') { echo '<div id="timedate"><strong>Time: </strong>10:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '23') { echo '<div id="timedate"><strong>Time: </strong>11:00 PM Local Time</div>'; } 
									if ($recurring_webinar_time == '24') { echo '<div id="timedate"><strong>Time: </strong>12:00 AM Local Time</div>'; } 
						?>
					<?php } 
					} 
				}


			?>
			
		<?php } ?>
		
		<!-- ONETIME WEBINAR ON -->
		
		<?php if(get_post_meta($post_id,'wpwebinar_onetime_webinar',true) == "on"){ ?>
		
		<div id="onetimedate" style="text-align: left; margin-bottom: 15px; margin-left: 20px;">
			<strong>Date:</strong> 
				<?php 	$webinarmonth = get_post_meta($post_id,'wpwebinar_webinar_month',true);
						if ($webinarmonth == '1') { echo 'January'; }
						if ($webinarmonth == '2') { echo 'February'; }
						if ($webinarmonth == '3') { echo 'March'; }
						if ($webinarmonth == '4') { echo 'April'; }
						if ($webinarmonth == '5') { echo 'May'; }
						if ($webinarmonth == '6') { echo 'June'; }
						if ($webinarmonth == '7') { echo 'July'; }
						if ($webinarmonth == '8') { echo 'August'; }
						if ($webinarmonth == '9') { echo 'September'; }			
						if ($webinarmonth == '10') { echo 'October'; }
						if ($webinarmonth == '11') { echo 'November'; }
						if ($webinarmonth == '12') { echo 'December'; }
				?>
				<?php echo get_post_meta($post_id,'wpwebinar_webinar_day',true); ?>, <?php echo get_post_meta($post_id,'wpwebinar_webinar_year',true); ?>
			</br><strong>Time:</strong>
				<?php 
				$admin_time = get_post_meta($post_id,'wpwebinar_admintime',true);
				$local_time = get_post_meta($post_id,'wpwebinar_localtime',true);
				$gmt_time_zone = get_post_meta($post_id,'wpwebinar_webinar_timezone',true);
				
					if ($local_time == 'on') {
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '1') { echo '1:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '2') { echo '2:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '3') { echo '3:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '4') { echo '4:00 AM Local Time'; }
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '5') { echo '5:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '6') { echo '6:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '7') { echo '7:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '8') { echo '8:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '9') { echo '9:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '10') { echo '10:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '11') { echo '11:00 AM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '12') { echo '12:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '13') { echo '1:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '14') { echo '2:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '15') { echo '3:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '16') { echo '4:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '17') { echo '5:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '18') { echo '6:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '19') { echo '7:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '20') { echo '8:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '21') { echo '9:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '22') { echo '10:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '23') { echo '11:00 PM Local Time'; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '24') { echo '12:00 AM Local Time'; } 
					}
					
					if ($admin_time == 'on') {

						if ($gmt_time_zone == "Pacific/Midway") { $gmt_time_zone_set = '(GMT-11:00) Midway Island, Samoa'; }
						if ($gmt_time_zone == "America/Adak") { $gmt_time_zone_set = '(GMT-10:00) Hawaii-Aleutian'; }
						if ($gmt_time_zone == "Etc/GMT+10") { $gmt_time_zone_set = '(GMT-10:00) Hawaii'; }
						if ($gmt_time_zone == "Pacific/Marquesas") { $gmt_time_zone_set = '(GMT-09:30) Marquesas Islands'; }
						if ($gmt_time_zone == "Pacific/Gambier") { $gmt_time_zone_set = '(GMT-09:00) Gambier Islands'; }
						if ($gmt_time_zone == "America/Anchorage") { $gmt_time_zone_set = '(GMT-09:00) Alaska'; }
						if ($gmt_time_zone == "America/Ensenada") { $gmt_time_zone_set = '(GMT-08:00) Tijuana, Baja California'; }
						if ($gmt_time_zone == "Etc/GMT+8") { $gmt_time_zone_set = '(GMT-08:00) Pitcairn Islands'; }
						if ($gmt_time_zone == "America/Los_Angeles") { $gmt_time_zone_set = '(GMT-08:00) Pacific Time (US & Canada)'; }
						if ($gmt_time_zone == "America/Denver") { $gmt_time_zone_set = '(GMT-07:00) Mountain Time (US & Canada)'; }
						if ($gmt_time_zone == "America/Chihuahua") { $gmt_time_zone_set = '(GMT-07:00) Chihuahua, La Paz, Mazatlan'; }
						if ($gmt_time_zone == "America/Dawson_Creek") { $gmt_time_zone_set = '(GMT-07:00) Arizona'; }
						if ($gmt_time_zone == "America/Belize") { $gmt_time_zone_set = '(GMT-06:00) Saskatchewan, Central America'; }
						if ($gmt_time_zone == "America/Cancun") { $gmt_time_zone_set = '(GMT-06:00) Guadalajara, Mexico City, Monterrey'; }
						if ($gmt_time_zone == "Chile/EasterIsland") { $gmt_time_zone_set = '(GMT-06:00) Easter Island'; }
						if ($gmt_time_zone == "America/Chicago") { $gmt_time_zone_set = '(GMT-06:00) Central Time (US & Canada)'; }
						if ($gmt_time_zone == "America/New_York") { $gmt_time_zone_set = '(GMT-05:00) Eastern Time (US & Canada)'; }
						if ($gmt_time_zone == "America/Havana") { $gmt_time_zone_set = '(GMT-05:00) Cuba'; }
						if ($gmt_time_zone == "America/Bogota") { $gmt_time_zone_set = '(GMT-05:00) Bogota, Lima, Quito, Rio Branco'; }
						if ($gmt_time_zone == "America/Caracas") { $gmt_time_zone_set = '(GMT-04:30) Caracas'; }
						if ($gmt_time_zone == "America/Santiago") { $gmt_time_zone_set = '(GMT-04:00) Santiago'; }
						if ($gmt_time_zone == "America/La_Paz") { $gmt_time_zone_set = '(GMT-04:00) La Paz'; }
						if ($gmt_time_zone == "Atlantic/Stanley") { $gmt_time_zone_set = '(GMT-04:00) Faukland Islands'; }
						if ($gmt_time_zone == "America/Campo_Grande") { $gmt_time_zone_set = '(GMT-04:00) Brazil'; }
						if ($gmt_time_zone == "America/Goose_Bay") { $gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Goose Bay)'; }
						if ($gmt_time_zone == "America/Glace_Bay") { $gmt_time_zone_set = '(GMT-04:00) Atlantic Time (Canada)'; }
						if ($gmt_time_zone == "America/St_Johns") { $gmt_time_zone_set = '(GMT-03:30) Newfoundland'; }
						if ($gmt_time_zone == "America/Araguaina") { $gmt_time_zone_set = '(GMT-03:00) UTC-3'; }
						if ($gmt_time_zone == "America/Montevideo") { $gmt_time_zone_set = '(GMT-03:00) Montevideo'; }
						if ($gmt_time_zone == "America/Miquelon") { $gmt_time_zone_set = '(GMT-03:00) Miquelon, St. Pierre'; }
						if ($gmt_time_zone == "America/Godthab") { $gmt_time_zone_set = '(GMT-03:00) Greenland'; }
						if ($gmt_time_zone == "America/Argentina/Buenos_Aires") { $gmt_time_zone_set = '(GMT-03:00) Buenos Aires'; }
						if ($gmt_time_zone == "America/Sao_Paulo") { $gmt_time_zone_set = '(GMT-03:00) Brasilia'; }
						if ($gmt_time_zone == "America/Noronha") { $gmt_time_zone_set = '(GMT-02:00) Mid-Atlantic'; }
						if ($gmt_time_zone == "Atlantic/Cape_Verde") { $gmt_time_zone_set = '(GMT-01:00) Cape Verde Is.'; }
						if ($gmt_time_zone == "Atlantic/Azores") { $gmt_time_zone_set = '(GMT-01:00) Azores'; }
						if ($gmt_time_zone == "Europe/Belfast") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Belfast'; }
						if ($gmt_time_zone == "Europe/Dublin") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Dublin'; }
						if ($gmt_time_zone == "Europe/Lisbon") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : Lisbon'; }
						if ($gmt_time_zone == "Europe/London") { $gmt_time_zone_set = '(GMT) Greenwich Mean Time : London'; }
						if ($gmt_time_zone == "Africa/Abidjan") { $gmt_time_zone_set = '(GMT) Monrovia, Reykjavik'; }
						if ($gmt_time_zone == "Europe/Amsterdam") { $gmt_time_zone_set = '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'; }
						if ($gmt_time_zone == "Europe/Belgrade") { $gmt_time_zone_set = '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'; }
						if ($gmt_time_zone == "Europe/Brussels") { $gmt_time_zone_set = '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'; }
						if ($gmt_time_zone == "Africa/Algiers") { $gmt_time_zone_set = '(GMT+01:00) West Central Africa'; }
						if ($gmt_time_zone == "Africa/Windhoek") { $gmt_time_zone_set = '(GMT+01:00) Windhoek'; }
						if ($gmt_time_zone == "Asia/Beirut") { $gmt_time_zone_set = '(GMT+02:00) Beirut'; }
						if ($gmt_time_zone == "Africa/Cairo") { $gmt_time_zone_set = '(GMT+02:00) Cairo'; }
						if ($gmt_time_zone == "Asia/Gaza") { $gmt_time_zone_set = '(GMT+02:00) Gaza'; }
						if ($gmt_time_zone == "Africa/Blantyre") { $gmt_time_zone_set = '(GMT+02:00) Harare, Pretoria'; }
						if ($gmt_time_zone == "Asia/Jerusalem") { $gmt_time_zone_set = '(GMT+02:00) Jerusalem'; }
						if ($gmt_time_zone == "Europe/Minsk") { $gmt_time_zone_set = '(GMT+02:00) Minsk'; }
						if ($gmt_time_zone == "Asia/Damascus") { $gmt_time_zone_set = '(GMT+02:00) Syria'; }
						if ($gmt_time_zone == "Europe/Moscow") { $gmt_time_zone_set = '(GMT+03:00) Moscow, St. Petersburg, Volgograd'; }
						if ($gmt_time_zone == "Africa/Addis_Ababa") { $gmt_time_zone_set = '(GMT+03:00) Nairobi'; }
						if ($gmt_time_zone == "Asia/Tehran") { $gmt_time_zone_set = '(GMT+03:30) Tehran'; }
						if ($gmt_time_zone == "Asia/Dubai") { $gmt_time_zone_set = '(GMT+04:00) Abu Dhabi, Muscat'; }
						if ($gmt_time_zone == "Asia/Yerevan") { $gmt_time_zone_set = '(GMT+04:00) Yerevan'; }
						if ($gmt_time_zone == "Asia/Kabul") { $gmt_time_zone_set = '(GMT+04:30) Kabul'; }
						if ($gmt_time_zone == "Asia/Yekaterinburg") { $gmt_time_zone_set = '(GMT+05:00) Ekaterinburg'; }
						if ($gmt_time_zone == "Asia/Tashkent") { $gmt_time_zone_set = '(GMT+05:00) Tashkent'; }
						if ($gmt_time_zone == "Asia/Kolkata") { $gmt_time_zone_set = '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'; }
						if ($gmt_time_zone == "Asia/Katmandu") { $gmt_time_zone_set = '(GMT+05:45) Kathmandu'; }
						if ($gmt_time_zone == "Asia/Dhaka") { $gmt_time_zone_set = '(GMT+06:00) Astana, Dhaka'; }
						if ($gmt_time_zone == "Asia/Novosibirsk") { $gmt_time_zone_set = '(GMT+06:00) Novosibirsk'; }
						if ($gmt_time_zone == "Asia/Rangoon") { $gmt_time_zone_set = '(GMT+06:30) Yangon (Rangoon)'; }
						if ($gmt_time_zone == "Asia/Bangkok") { $gmt_time_zone_set = '(GMT+07:00) Bangkok, Hanoi, Jakarta'; }
						if ($gmt_time_zone == "Asia/Krasnoyarsk") { $gmt_time_zone_set = '(GMT+07:00) Krasnoyarsk'; }
						if ($gmt_time_zone == "Asia/Hong_Kong") { $gmt_time_zone_set = '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'; }
						if ($gmt_time_zone == "Asia/Irkutsk") { $gmt_time_zone_set = '(GMT+08:00) Irkutsk, Ulaan Bataar'; }
						if ($gmt_time_zone == "Australia/Perth") { $gmt_time_zone_set = '(GMT+08:00) Perth'; }
						if ($gmt_time_zone == "Australia/Eucla") { $gmt_time_zone_set = '(GMT+08:45) Eucla'; }
						if ($gmt_time_zone == "Asia/Tokyo") { $gmt_time_zone_set = '(GMT+09:00) Osaka, Sapporo, Tokyo'; }
						if ($gmt_time_zone == "Asia/Seoul") { $gmt_time_zone_set = '(GMT+09:00) Seoul'; }
						if ($gmt_time_zone == "Asia/Yakutsk") { $gmt_time_zone_set = '(GMT+09:00) Yakutsk'; }
						if ($gmt_time_zone == "Australia/Adelaide") { $gmt_time_zone_set = '(GMT+09:30) Adelaide'; }
						if ($gmt_time_zone == "Australia/Darwin") { $gmt_time_zone_set = '(GMT+09:30) Darwin'; }
						if ($gmt_time_zone == "Australia/Brisbane") { $gmt_time_zone_set = '(GMT+10:00) Brisbane'; }
						if ($gmt_time_zone == "Australia/Hobart") { $gmt_time_zone_set = '(GMT+10:00) Hobart'; }
						if ($gmt_time_zone == "Asia/Vladivostok") { $gmt_time_zone_set = '(GMT+10:00) Vladivostok'; }
						if ($gmt_time_zone == "Australia/Lord_Howe") { $gmt_time_zone_set = '(GMT+10:30) Lord Howe Island'; }
						if ($gmt_time_zone == "Etc/GMT-11") { $gmt_time_zone_set = '(GMT+11:00) Solomon Is., New Caledonia'; }
						if ($gmt_time_zone == "Asia/Magadan") { $gmt_time_zone_set = '(GMT+11:00) Magadan'; }
						if ($gmt_time_zone == "Pacific/Norfolk") { $gmt_time_zone_set = '(GMT+11:30) Norfolk Island'; }
						if ($gmt_time_zone == "Asia/Anadyr") { $gmt_time_zone_set = '(GMT+12:00) Anadyr, Kamchatka'; }
						if ($gmt_time_zone == "Pacific/Auckland") { $gmt_time_zone_set = '(GMT+12:00) Auckland, Wellington'; }
						if ($gmt_time_zone == "Etc/GMT-12") { $gmt_time_zone_set = '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'; }
						if ($gmt_time_zone == "Pacific/Chatham") { $gmt_time_zone_set = '(GMT+12:45) Chatham Islands'; }
						if ($gmt_time_zone == "Pacific/Tongatapu") { $gmt_time_zone_set = '(GMT+13:00) Nuku\'alofa'; }
						if ($gmt_time_zone == "Pacific/Kiritimati") { $gmt_time_zone_set = '(GMT+14:00) Kiritimati'; }
					
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '1') { echo '1:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; }
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '2') { echo '2:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '3') { echo '3:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '4') { echo '4:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; }
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '5') { echo '5:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '6') { echo '6:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '7') { echo '7:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '8') { echo '8:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '9') { echo '9:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '10') { echo '10:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '11') { echo '11:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '12') { echo '12:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '13') { echo '1:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '14') { echo '2:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '15') { echo '3:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '16') { echo '4:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '17') { echo '5:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '18') { echo '6:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '19') { echo '7:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '20') { echo '8:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '21') { echo '9:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '22') { echo '10:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '23') { echo '11:00 PM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
						if (get_post_meta($post_id,'wpwebinar_webinar_hour',true) == '24') { echo '12:00 AM'; echo '</br><strong>Time Zone:</strong></br>'; echo $gmt_time_zone_set; } 
					}
					
					
				?>
		</div>
	
		<?php } ?>
			
		<!-- ****************************************************************** -->
		<!-- AUTORESPONDER -->
		<!-- ****************************************************************** -->

		<!-- ERASES NAME AND EMAIL INFO -->
		<script type="text/javascript"> function make_blank() { document.wpwebinar.first_name.value =""; document.optin.email.value =""; } </script>
		
		<script type="text/javascript">
			function clickclear(thisfield, defaulttext) {
				if (thisfield.value == defaulttext) {
				thisfield.value = "";
				}
			}

			function clickrecall(thisfield, defaulttext) {
				if (thisfield.value == "") {
				thisfield.value = defaulttext;
				}
			}
		</script>
			
<?php
global $post;
$post_id = $post->ID;
$formURL = get_post_meta($post_id,'wpwebinar_optin_form_url',true);
$emailField = get_post_meta($post_id,'wpwebinar_optin_email_field',true);
$nameField = get_post_meta($post_id,'wpwebinar_optin_name_field',true);
$hiddenFields = get_post_meta($post_id,'wpwebinar_optin_hidden_field',true);
$buttontext = get_post_meta($post_id,'wpwebinar_button_text',true);
?> 

				<?php if ($moderntheme == 'on') { 
					echo '<div class="optintext"><b>3.)</b> Where Do We Send The Invite?</div>';
					} ?>
			<input type="text" class="name" name="first_name" id="first_name" value="Enter Your First Name" onblur="copyName();clickrecall(this,'Enter Your First Name');" onFocus="javascript:if(this.value=='Enter Your First Name') this.value='';"/>  
			<input type="text" class="email" name="email" id="email" value="Type In Your Email Address" onblur="copyEmail();clickrecall(this,'Type In Your Email Address');" onFocus="javascript:if(this.value=='Type In Your Email Address') this.value='';"/>
			<input type="submit" name="go" id="go" class="optinpagebutton" value="<?php echo $buttontext; ?>"/>
			</form>
	
  
			<form id="form2" name="form2" method="post" action="<?php echo $formURL; ?>">
				<input type="hidden" id="autoname" name="<?php echo $nameField; ?>" value='First Name'/>
				<input type="hidden" id="autoemail" name="<?php echo $emailField; ?>" value='Email'/>
				<?php echo $hiddenFields; ?>
			</form>

	</div><!-- END WEBINAR SIDEBAR -->
	<div style="clear:both"></div>	

		<?php
		global $post;
		$post_id = $post->ID;
			echo '<footer>';
			echo wpautop(get_post_meta($post_id,'wpwebinar_footer_text',true));
			echo '</footer>';
		?>


</div><!-- END WRAPPER OPTIN -->
<div style="clear:both"></div>	
<?php } ?>


<?php
global $post;
$post_id = $post->ID;
$values = $_GET["webinar"];
$webinartime = $_GET["time"];
$webinardate = $_GET["date"];
$type = $_GET["type"];
$emailto = $_GET["email"];
$person = $_GET["first_name"];


// SETS LOCAL TIME ZONE
if($values == "processing1"){
	if(!isset($_SESSION['timezone']))
	{
		if(!isset($_REQUEST['offset']))
		{
		?>
			<script>
			var d = new Date()
			var offset= -d.getTimezoneOffset()/60;
			location.href = "<?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar=processing2&type=<?php echo $_GET["type"]; ?>&date=<?php echo $_GET["date"]; ?>&time=<?php echo $_GET["time"]; ?>&email=<?php echo $emailto; ?>&person=<?php echo $person; ?>&offset="+offset;

			</script>
			<?php   
		}
		else
		{
			$zonelist = array('Kwajalein' => -12.00, 'Pacific/Midway' => -11.00, 'Pacific/Honolulu' => -10.00, 'America/Anchorage' => -9.00, 'America/Los_Angeles' => -8.00, 'America/Denver' => -7.00, 'America/Tegucigalpa' => -6.00, 'America/New_York' => -5.00, 'America/Caracas' => -4.30, 'America/Halifax' => -4.00, 'America/St_Johns' => -3.30, 'America/Argentina/Buenos_Aires' => -3.00, 'America/Sao_Paulo' => -3.00, 'Atlantic/South_Georgia' => -2.00, 'Atlantic/Azores' => -1.00, 'Europe/Dublin' => 0, 'Europe/Belgrade' => 1.00, 'Europe/Minsk' => 2.00, 'Asia/Kuwait' => 3.00, 'Asia/Tehran' => 3.30, 'Asia/Muscat' => 4.00, 'Asia/Yekaterinburg' => 5.00, 'Asia/Kolkata' => 5.30, 'Asia/Katmandu' => 5.45, 'Asia/Dhaka' => 6.00, 'Asia/Rangoon' => 6.30, 'Asia/Krasnoyarsk' => 7.00, 'Asia/Brunei' => 8.00, 'Asia/Seoul' => 9.00, 'Australia/Darwin' => 9.30, 'Australia/Canberra' => 10.00, 'Asia/Magadan' => 11.00, 'Pacific/Fiji' => 12.00, 'Pacific/Tongatapu' => 13.00);
			$index = array_keys($zonelist, $_REQUEST['offset']);
			$_SESSION['timezone'] = $index[0];
		}
	}
	date_default_timezone_set($_SESSION['timezone']);
}
?>



<?php 
global $post;
$post_id = $post->ID;
$values = $_GET["webinar"];
$webinartime = $_GET["time"];
$webinardate = $_GET["date"];
$type = $_GET["type"];
$emailto = $_GET["email"];

$from_name = get_post_meta($post_id,'wpwebinar_from_name',true);
$from_email = get_post_meta($post_id,'wpwebinar_from_email',true);

$subject_one = get_post_meta($post_id,'wpwebinar_email_one_subject',true);
$message_one = get_post_meta($post_id,'wpwebinar_email_one_message',true);

$subject_1before = get_post_meta($post_id,'wpwebinar_email_1hour_subject',true);
$message_1before = get_post_meta($post_id,'wpwebinar_email_1hour_message',true);

$subject_24before = get_post_meta($post_id,'wpwebinar_email_24hour_subject',true);
$message_24before = get_post_meta($post_id,'wpwebinar_email_24hour_message',true);

$subject_replay = get_post_meta($post_id,'wpwebinar_email_replay_subject',true);
$message_replay = get_post_meta($post_id,'wpwebinar_email_replay_message',true);

$linkperm =  get_permalink( $_REQUEST['posted'] ); 
$thankyoutype = '?webinar=wait&type=';
$anddate = '&date='; 
$andtime = '&time='; 

$webinarlink = $linkperm.$thankyoutype.$type.$anddate.$webinardate.$andtime.$webinartime;
$insertdate = date("l F jS, Y", strtotime($webinardate));

if ($webinartime == '1') { $newwebinartime = '1:00 AM Local Time'; } 
if ($webinartime == '2') { $newwebinartime =  '2:00 AM Local Time'; } 
if ($webinartime == '3') { $newwebinartime =  '3:00 AM Local Time'; } 
if ($webinartime == '4') { $newwebinartime =  '4:00 AM Local Time'; }
if ($webinartime == '5') { $newwebinartime =  '5:00 AM Local Time'; } 
if ($webinartime == '6') { $newwebinartime =  '6:00 AM Local Time'; } 
if ($webinartime == '7') { $newwebinartime =  '7:00 AM Local Time'; } 
if ($webinartime == '8') { $newwebinartime =  '8:00 AM Local Time'; } 
if ($webinartime == '9') { $newwebinartime =  '9:00 AM Local Time'; } 
if ($webinartime == '10') { $newwebinartime =  '10:00 AM Local Time'; } 
if ($webinartime == '11') { $newwebinartime =  '11:00 AM Local Time'; } 
if ($webinartime == '12') { $newwebinartime =  '12:00 PM Local Time'; } 
if ($webinartime == '13') { $newwebinartime =  '1:00 PM Local Time'; } 
if ($webinartime == '14') { $newwebinartime =  '2:00 PM Local Time'; } 
if ($webinartime == '15') { $newwebinartime =  '3:00 PM Local Time'; } 
if ($webinartime == '16') { $newwebinartime =  '4:00 PM Local Time'; } 
if ($webinartime == '17') { $newwebinartime =  '5:00 PM Local Time'; } 
if ($webinartime == '18') { $newwebinartime =  '6:00 PM Local Time'; } 
if ($webinartime == '19') { $newwebinartime =  '7:00 PM Local Time'; } 
if ($webinartime == '20') { $newwebinartime =  '8:00 PM Local Time'; } 
if ($webinartime == '21') { $newwebinartime =  '9:00 PM Local Time'; } 
if ($webinartime == '22') { $newwebinartime =  '10:00 PM Local Time'; } 
if ($webinartime == '23') { $newwebinartime =  '11:00 PM Local Time'; } 
if ($webinartime == '24') { $newwebinartime =  '12:00 AM Local Time'; } 

// Message One
$new_message_one = str_replace("%WEBINARLINK%", $webinarlink, $message_one);
$new_message_two = str_replace("%WEBINARDATE%", $insertdate, $new_message_one);
$new_message_final = str_replace("%WEBINARTIME%", $newwebinartime, $new_message_two);

// Message 1 Hour
$new_message_1before = str_replace("%WEBINARLINK%", $webinarlink, $message_1before);
$new_message_1before_two = str_replace("%WEBINARDATE%", $insertdate, $new_message_1before);
$new_message_1before_final = str_replace("%WEBINARTIME%", $newwebinartime, $new_message_1before_two);

// Message 24
$new_message_24before = str_replace("%WEBINARLINK%", $webinarlink, $message_24before);
$new_message_24before_two = str_replace("%WEBINARDATE%", $insertdate, $new_message_24before);
$new_message_24before_final = str_replace("%WEBINARTIME%", $newwebinartime, $new_message_24before_two);

// Message Replay
$new_message_replay = str_replace("%WEBINARLINK%", $webinarlink, $message_replay);
$new_message_replay_two = str_replace("%WEBINARDATE%", $insertdate, $new_message_replay);
$new_message_replay_final = str_replace("%WEBINARTIME%", $newwebinartime, $new_message_replay_two);


if($values == "processing2"){ ?>

<?php 

			$zonelist = array('Kwajalein' => -12.00, 'Pacific/Midway' => -11.00, 'Pacific/Honolulu' => -10.00, 'America/Anchorage' => -9.00, 'America/Los_Angeles' => -8.00, 'America/Denver' => -7.00, 'America/Tegucigalpa' => -6.00, 'America/New_York' => -5.00, 'America/Caracas' => -4.30, 'America/Halifax' => -4.00, 'America/St_Johns' => -3.30, 'America/Argentina/Buenos_Aires' => -3.00, 'America/Sao_Paulo' => -3.00, 'Atlantic/South_Georgia' => -2.00, 'Atlantic/Azores' => -1.00, 'Europe/Dublin' => 0, 'Europe/Belgrade' => 1.00, 'Europe/Minsk' => 2.00, 'Asia/Kuwait' => 3.00, 'Asia/Tehran' => 3.30, 'Asia/Muscat' => 4.00, 'Asia/Yekaterinburg' => 5.00, 'Asia/Kolkata' => 5.30, 'Asia/Katmandu' => 5.45, 'Asia/Dhaka' => 6.00, 'Asia/Rangoon' => 6.30, 'Asia/Krasnoyarsk' => 7.00, 'Asia/Brunei' => 8.00, 'Asia/Seoul' => 9.00, 'Australia/Darwin' => 9.30, 'Australia/Canberra' => 10.00, 'Asia/Magadan' => 11.00, 'Pacific/Fiji' => 12.00, 'Pacific/Tongatapu' => 13.00);
			$index = array_keys($zonelist, $_REQUEST['offset']);
			$_SESSION['timezone'] = $index[0];

	date_default_timezone_set($_SESSION['timezone']);

?>

<?php
// Initial Email
wp_mail(
   ''.$emailto.'',
   ''.$subject_one.'',
   ''.$new_message_final.'',
	array(
	'From: '.$from_name.' <'.$from_email.'>',
	)
);

// 1 Hour Before
wp_delayed_mail(
   strtotime ( '-1 hours' , strtotime ( "$webinardate $webinartime:00" ) ), 
   ''.$emailto.'',
   ''.$subject_1before.'',
   ''.$new_message_1before_final.'',
   array(
      'From: '.$from_name.' <'.$from_email.'>',
   )
);

// 24 Hours Before
wp_delayed_mail(
   strtotime ( '-1 days' , strtotime ( "$webinardate $webinartime:00" ) ), 
   ''.$emailto.'',
   ''.$subject_24before.'',
   ''.$new_message_24before_final.'',
   array(
      'From: '.$from_name.' <'.$from_email.'>',
   )
);

// 24 Hours After
wp_delayed_mail(
   strtotime ( '+1 days' , strtotime ( "$webinardate $webinartime:00" ) ), 
   ''.$emailto.'',
   ''.$subject_replay.'',
   ''.$new_message_replay_final.'',
   array(
      'From: '.$from_name.' <'.$from_email.'>',
   )
);
?>
	
	<!-- ****************************************************************** -->
	<!-- WEBINAR IN BETWEEN PAGE -->
	<!-- ****************************************************************** -->
	
	<script type="text/javascript">
		<!--
		setTimeout("window.location = '<?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar=thankyou&type=<?php echo $_GET["type"]; ?>&date=<?php echo $_GET["date"]; ?>&time=<?php echo $_GET["time"]; ?>&email=<?php echo $_GET["email"]; ?>&person=<?php echo $_GET["person"]; ?>';",5000);
		//-->
	</script>
	
	<div id="webinar_wrapper"><!-- START WEBINAR WRAPPER BETWEEN PAGE -->
	<div id="waiting">
		<div id="loading2"style="font-size: 24px; font-weight: bold; text-align: center;"><img src="<?php echo $wp_plugin_url; echo '/wp-webinar/images/generator.gif'?>" width="100" height="100"/> </div>
		<div id="wait" style="font-size: 24px; font-weight: bold; text-align: center;">Please Wait While We Process Your Registration!</div>
	</div>
	
	</div><!-- END WRAPPER THANK YOU PAGE -->
<?php } ?>


<!-- NEW THANK YOU PAGE -->

<?php 
global $post;
$post_id = $post->ID;
$values = $_GET["webinar"];
$webinartime = $_GET["time"];
$webinardate = $_GET["date"];
$webinaremail = $_GET["email"];
$wbdates = strtotime($webinardate); 
$type = $_GET["type"];
$person = $_GET["person"];
if($values == "thankyou"){ ?>
	<!-- Countdown dashboard start -->
	<!-- ****************************************************************** -->
	<!-- WEBINAR THANK YOU PAGE -->
	<!-- ****************************************************************** -->
	<div id="webinar_wrapper"><!-- START WRAPPER THANK YOU -->
	
		<div id="webinar_thankyou_side">
		<h2>Your Webinar Details:</h2>
			<div id="webinar_details_side">
			
				<strong>Name: </strong><?php echo $person ?></br>
				<strong>Email: </strong><?php echo $webinaremail ?></br>
				<strong>Webinar Date: </strong><?php echo date("F jS, Y", strtotime($webinardate)); ?></br>

				<strong>Webinar Time: </strong><?php 
					if ($webinartime == '1') { echo '1:00 AM'; } 
					if ($webinartime == '2') { echo '2:00 AM'; } 
					if ($webinartime == '3') { echo '3:00 AM'; } 
					if ($webinartime == '4') { echo '4:00 AM'; }
					if ($webinartime == '5') { echo '5:00 AM'; } 
					if ($webinartime == '6') { echo '6:00 AM'; } 
					if ($webinartime == '7') { echo '7:00 AM'; } 
					if ($webinartime == '8') { echo '8:00 AM'; } 
					if ($webinartime == '9') { echo '9:00 AM'; } 
					if ($webinartime == '10') { echo '10:00 AM'; } 
					if ($webinartime == '11') { echo '11:00 AM'; } 
					if ($webinartime == '12') { echo '12:00 PM'; } 
					if ($webinartime == '13') { echo '1:00 PM'; } 
					if ($webinartime == '14') { echo '2:00 PM'; } 
					if ($webinartime == '15') { echo '3:00 PM'; } 
					if ($webinartime == '16') { echo '4:00 PM'; } 
					if ($webinartime == '17') { echo '5:00 PM'; } 
					if ($webinartime == '18') { echo '6:00 PM'; } 
					if ($webinartime == '19') { echo '7:00 PM'; } 
					if ($webinartime == '20') { echo '8:00 PM'; } 
					if ($webinartime == '21') { echo '9:00 PM'; } 
					if ($webinartime == '22') { echo '10:00 PM'; } 
					if ($webinartime == '23') { echo '11:00 PM'; } 
					if ($webinartime == '24') { echo '12:00 AM'; } ?></br>
			
			</div>
			
			<div id="webinar_step">
				<img src="<?php echo $wp_plugin_url; echo '/wp-webinar/images/printit.png'?>" width="280" height="48"/></br>
				<p>Click the link below to print this page and be sure to set it in a place where you can see it to remind you of this webinar. </p>
				<center><strong><A class="printbutton" HREF="javascript:window.print()">Click Here to Print This Page</A></strong></center>
			</div>
			
			<div id="webinar_step">
				<img src="<?php echo $wp_plugin_url; echo '/wp-webinar/images/shareit.png'?>" width="280" height="48"/></br>
				<p>Share this webinar with your friends. Click the buttons below to share it now.</p>
				
				<div id="media_wrapper" style="width: 100%; margin: 0 auto;">
				
				<div id="socbuttons">
					<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
					<script type="IN/Share" data-url="<?php echo get_permalink( $_REQUEST['posted'] ); ?>"></script>
				</div>

				<!-- FACEBOOK CODE -->
				<div id="socbuttons">
					<iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fdomain.com&layout=button&size=small&mobile_iframe=true&appId=340767419314451&width=59&height=20" width="59" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
				
				<!-- TWITTER CODE -->
				<div id="socbuttons">
					<div id= "webinar_media_button" style="margin-right: 18px;">
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-url="<?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar" data-text="I just signed up for an amazing webinar. Here's the link: <?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div>
				</div>
				
				<!-- GOOGLE+ CODE -->
				<div id="socbuttons">
					<script src="https://apis.google.com/js/platform.js" async defer></script>
					<div class="g-plus" data-action="share" data-width="120" data-height="24" data-href="<?php echo get_permalink( $_REQUEST['posted'] ); ?>"></div>
				</div>
				
				</div>
			</div>
		
		</div>
		
		<div id="webinar_content">
			<div id="webinar_video">
					
				<!-- ****************************************************************** -->
				<!-- Thank You Page Text -->
				<!-- ****************************************************************** -->
				<h2>Congratulations You're Registered!</h2>
				
				<?php 
				global $post;
				$post_id = $post->ID;
				$values = $_GET["webinar"];

				$thankyou_video_url = get_post_meta($post_id,'wpwebinar_thankyou_page_video_url',true);
				$thankyou_video_width = get_post_meta($post_id,'wpwebinar_thankyou_page_video_width',true);
				$thankyou_video_height = get_post_meta($post_id,'wpwebinar_thankyou_page_video_height',true);

				?>
				
				<!-- ****************************************************************** -->
				<!-- THANK YOU PAGE VIDEO -->
				<!-- ****************************************************************** -->
				
<!-- <style>
.flowplayer {
   width: <?php echo $thankyou_video_width; ?>px;
   height: <?php echo $thankyou_video_height; ?>px;
}
</style>
	<script>
	flowplayer.conf = {
	   autoplay: true,
	   share: false,
	};
	</script>
	<div class="flowplayer">
	   <video>
	      <source type="video/mp4"
	              src="<?php echo $thankyou_video_url; ?>">
	      </video>
	</div>	
-->

<link href="https://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

  <video autoplay id="my-video" class="video-js" controls preload="auto" width="<?php echo $thankyou_video_width; ?>" height="<?php echo $thankyou_video_height; ?>"
  poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
    <source src="<?php echo $thankyou_video_url; ?>" type='video/mp4'>
  </video>

<script src="https://vjs.zencdn.net/6.6.3/video.js"></script>

		
			</div>
			<div style="clear: both;"></div>

			<div id="thankyoutext">
				<?php
				global $post;
				$post_id = $post->ID;
				echo wpautop(get_post_meta($post_id,'wpwebinar_thankyou_page_content',true));
				?>
			</div>
			
			<div id="webinar_link">
			<h2>Here's Your Webinar Link:</h2>
			<?php 
				$shorty = get_permalink( $_REQUEST['posted'] ) . '?webinar=wait&type=' . $_GET['type'] . '&date=' . $_GET['date'] . '&time=' . $_GET['time'];
				$turl = getTinyUrl($shorty);
				echo '<a href="'.$turl.'">'.$turl.'</a>'
			?>
			</div>
		</div>
			<div style="clear: both;"></div>
			
			

	</div><!-- END WRAPPER THANK YOU PAGE -->
	<div style="clear: both;"></div>
<?php } ?>

<!-- END NEW THANK YOU PAGE -->


<?php 
global $post;
$post_id = $post->ID;
$values = $_GET["webinar"];
$webinartime = $_GET["time"];
$webinardate = $_GET["date"];
$wbdates = strtotime($webinardate); 
$type = $_GET["type"];
if($values == "wait"){ ?>
	<!-- Countdown dashboard start -->
	<!-- ****************************************************************** -->
	<!-- WEBINAR THANK YOU PAGE -->
	<!-- ****************************************************************** -->
	<div id="webinar_wrapper"><!-- START WRAPPER THANK YOU -->
		<div id="countdown_dashboard" style="width: 600px; margin: 0 auto; padding-right: 20px;">

			<div class="dash days_dash">
				<span class="dash_title">days</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash hours_dash">
				<span class="dash_title">hours</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash minutes_dash">
				<span class="dash_title">minutes</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

			<div class="dash seconds_dash">
				<span class="dash_title">seconds</span>
				<div class="digit">0</div>
				<div class="digit">0</div>
			</div>

		</div>

		<?php if($type == "o"){ ?>

			<script language="javascript" type="text/javascript">
				jQuery(document).ready(function() {
					$('#countdown_dashboard').countDown({
						targetDate: {
							'day': 		<?php echo get_post_meta($post_id,'wpwebinar_webinar_day',true); ?>,
							'month': 	<?php echo get_post_meta($post_id,'wpwebinar_webinar_month',true); ?>,
							'year': 	<?php echo get_post_meta($post_id,'wpwebinar_webinar_year',true); ?>,
							'hour': 	<?php echo get_post_meta($post_id,'wpwebinar_webinar_hour',true); ?>,
							'min': 		0,
							'sec': 		0,

						},
					// onComplete function
					onComplete: function() { window.location.href='<?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar=play&date=<?php echo $webinardate; ?>&time=<?php echo $webinartime; ?>' }
					});
				});
			</script>

		<?php } ?>

		<?php if($type == "r"){ ?>

			<script language="javascript" type="text/javascript">
				jQuery(document).ready(function($) {
					$('#countdown_dashboard').countDown({
						targetDate: {
							'day': 		<?php echo date('d',$wbdates); ?>,
							'month': 	<?php echo date('m',$wbdates); ?>,
							'year': 	<?php echo date('Y',$wbdates); ?>,
							'hour': 	<?php echo $webinartime; ?>,
							'min': 		0,
							'sec': 		0
						},
						// onComplete function
						onComplete: function() { window.location.href='<?php echo get_permalink( $_REQUEST['posted'] ); ?>?webinar=play&date=<?php echo $webinardate; ?>&time=<?php echo $webinartime; ?>' }
					});
				});
			</script>

		<?php } ?>
		
		<!-- ****************************************************************** -->
		<!-- Thank You Page Text -->
		<!-- ****************************************************************** -->

		<?php
		global $post;
		$post_id = $post->ID;
			echo '<div style="clear:both">';
			echo '<div id="thankyou">';
			echo '<center><h2>The Webinar Will Start When The Countdown Reaches Zero</h2></center>';
			echo '</div>';
		?>
		

</div><!-- END WRAPPER THANK YOU PAGE -->
<?php } ?>

<?php 
global $post;
$post_id = $post->ID;
$values = $_GET["webinar"];
$webinartime = $_GET["time"];
$webinardate = $_GET["date"];
$webinar_video_url = get_post_meta($post_id,'wpwebinar_webinar_video_url',true);
$webinar_video_width = get_post_meta($post_id,'wpwebinar_webinar_video_width',true);
$webinar_video_height = get_post_meta($post_id,'wpwebinar_webinar_video_height',true);
$webinarminutes = get_post_meta($post_id,'wpwebinar_webinar_minutes',true);
$webinarseconds = get_post_meta($post_id,'wpwebinar_webinar_seconds',true);
$webinarplaytime = ($webinarminutes * 60) + $webinarseconds;

$minutes = get_post_meta($post_id,'wpwebinar_minutes',true);
$seconds = get_post_meta($post_id,'wpwebinar_seconds',true);
$delay = ($minutes * 60) + $seconds;
$playtimezone = get_post_meta($post_id,'wpwebinar_webinar_timezone',true);
$nowdate = date('Y-m-d G:i:s', strtotime("now"));
$originaltz = DateTime::createFromFormat( "Y-m-d G:i:s", "$nowdate", new DateTimeZone("UTC") );
$newtz = $originaltz;
$newtz->setTimeZone(new DateTimeZone("$playtimezone"));
$playtimenow = $newtz->format('Y-m-d G:i:s');
$currenttime = strtotime($playtimenow);
$webinarstart = strtotime("$webinardate $webinartime:00:00");
$delayseconds = $currenttime - $webinarstart;

if($values == "play"){ 

	if ($webinarplaytime < $delayseconds) { ?>
		<h1 style="text-align:center;">Sorry This Webinar Has Ended</h1>
	<? }

	else  { ?>
		<div id="webinar_wrapper"><!-- START WRAPPER PLAY REPLAY -->
				<!-- ****************************************************************** -->
				<!-- WEBINAR PAGE -->
				<!-- ****************************************************************** -->

		<link href="https://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
		<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

		<center><video autoplay id="video" class="video-js" preload="auto" width="<?php echo $webinar_video_width; ?>" height="<?php echo $webinar_video_height; ?>"
		  data-setup="{}">
			<source src="<?php echo $webinar_video_url; ?>" type='video/mp4'>
		</video></center>

		<script src="https://vjs.zencdn.net/6.6.3/video.js"></script>
		<link rel="stylesheet" href='<?php echo $wp_plugin_url; echo "/wp-webinar/videojs/"; ?>videojs-overlay.css'>
		<script src="//players.brightcove.net/videojs-overlay/2/videojs-overlay.min.js"></script>

		<script id="pageScript" type="text/javascript">
		  var myPlayer = videojs("video");
		  myPlayer.overlay({
		  "overlays": [
			{
		      content: '<a class="obutton" target="_blank" href="<?php echo get_post_meta($post_id,'wpwebinar_cta_url',true); ?>"><?php echo get_post_meta($post_id,'wpwebinar_cta_button_text',true); ?></a>',
		      start: <?php echo $delay; ?>,
		      align: 'bottom',
		      showBackground: false,
		    }
		  ]
		});
		</script>

		<?php 
		$webinardate = $_GET["date"];
			if($webinardate != ""){ ?>
		<script>
			var player = videojs("video");
			player.play();
			player.currentTime(<?php echo $delayseconds; ?>);
		</script>
		<?php } ?>

		<div id="info" style="display: none; margin-top: 25px;">
		<?php echo get_post_meta($post_id,'wpwebinar_call_to_action',true); ?>
		</div>
			</div><!-- END WRAPPER PLAY REPLAY -->
		<div style="clear:both"></div>	

<?php } 
}

if($values == "replay"){ ?>
		<div id="webinar_wrapper"><!-- START WRAPPER PLAY REPLAY -->
				<!-- ****************************************************************** -->
				<!-- WEBINAR PAGE -->
				<!-- ****************************************************************** -->

		<link href="https://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
		<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

		<center><video autoplay id="video" class="video-js" preload="auto" width="<?php echo $webinar_video_width; ?>" height="<?php echo $webinar_video_height; ?>"
		  data-setup="{}">
			<source src="<?php echo $webinar_video_url; ?>" type='video/mp4'>
		</video></center>

		<script src="https://vjs.zencdn.net/6.6.3/video.js"></script>
		<link rel="stylesheet" href='<?php echo $wp_plugin_url; echo "/wp-webinar/videojs/"; ?>videojs-overlay.css'>
		<script src="//players.brightcove.net/videojs-overlay/2/videojs-overlay.min.js"></script>

		<script id="pageScript" type="text/javascript">
		  var myPlayer = videojs("video");
		  myPlayer.overlay({
		  "overlays": [
			{
		      content: '<a class="obutton" target="_blank" href="<?php echo get_post_meta($post_id,'wpwebinar_cta_url',true); ?>"><?php echo get_post_meta($post_id,'wpwebinar_cta_button_text',true); ?></a>',
		      start: <?php echo $delay; ?>,
		      align: 'bottom',
		      showBackground: false,
		    }
		  ]
		});
		</script>

		<?php 
		$webinardate = $_GET["date"];
			if($webinardate != ""){ ?>
		<script>
			var player = videojs("video");
			player.play();
			player.currentTime(<?php echo $delayseconds; ?>);
		</script>
		<?php } ?>

		<div id="info" style="display: none; margin-top: 25px;">
		<?php echo get_post_meta($post_id,'wpwebinar_call_to_action',true); ?>
		</div>
			</div><!-- END WRAPPER PLAY REPLAY -->
		<div style="clear:both"></div>	

<?php } ?> 

<?php 
global $post;
$post_id = $post->ID;
$yourtheme = get_post_meta($post_id,'wpwebinar_theme_yours',true);
$basictheme = get_post_meta($post_id,'wpwebinar_theme_basic',true);
$affiliateid = get_post_meta($post_id,'wpwebinar_affiliateid',true);


	if ($yourtheme == 'on') {
		get_footer();
	}
	if ($basictheme == 'on') { 
		echo '</div></body></html>';
	}
	if ($moderntheme == 'on') {
		echo '</div></body></html>';
	}
?>