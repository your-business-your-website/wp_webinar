<?php
/*
Plugin Name: WP Webinar
Plugin URI: http://www.wpwebinar.com/
Description: Quickly and easily creates webinar replay pages. No Branding.
Version: 1.5.3
Author: Armand Morin
Author URI: http://www.ArmandMorin.com/
*/

// *********************************************************************************** //
// UPDATE CHECKS
// *********************************************************************************** //
require 'plugin-updates/plugin-update-checker.php';
$ExampleUpdateChecker = new PluginUpdateChecker(
	'http://wp-webinar.s3.amazonaws.com/wpwebinar-updates.json',
	__FILE__,
	'wp-webinar'
);

function getTinyUrl( $url ) {
	$tinyurl = file_get_contents( 'http://tinyurl.com/api-create.php?url=' . $url );
	return $tinyurl;
}

// *********************************************************************************** //
// CREATE CUSTOM POST TYPE
// *********************************************************************************** //

add_action( 'init', 'wpwebinar_init' );

function wpwebinar_init() {
	 $labels = array(
		 'name'               => _x( 'WP Webinar', 'post type general name' ),
		 'singular_name'      => _x( 'Webinar', 'post type singular name' ),
		 'add_new'            => _x( 'Add New', 'webinar' ),
		 'add_new_item'       => __( 'Add Webinar Page' ),
		 'edit_item'          => __( 'Edit Webinar Page' ),
		 'new_item'           => __( 'New Webinar Page' ),
		 'view_item'          => __( 'View Webinar Pages' ),
		 'search_items'       => __( 'Search Webinar Pages' ),
		 'not_found'          => __( 'No Webinars found' ),
		 'not_found_in_trash' => __( 'No Webinars pages found in Trash' ),
		 'parent_item_colon'  => '',
		 'menu_name'          => 'WP Webinar',

	 );
	 $args = array(
		 'labels'             => $labels,
		 'public'             => true,
		 'publicly_queryable' => true,
		 'show_ui'            => true,
		 'show_in_menu'       => true,
		 'query_var'          => true,
		 'rewrite'            => array( 'slug' => 'webinar' ),
		 '_builtin'           => false,
		 'capability_type'    => 'post',
		 'has_archive'        => false,
		 'hierarchical'       => false,
		 'menu_position'      => null,
		 'show_in_rest'       => true,
		 'supports'           => array( 'title' ),
	 );
	 register_post_type( 'wpwebinar', $args );
	 flush_rewrite_rules();
}

require_once 'mail/wp-delayed-mail.php';

// *********************************************************************************** //
// ADDS POST TYPE TO READING SETTINGS AND CORRECTS FRONT PAGE VIEW
// *********************************************************************************** //

add_filter( 'get_pages', 'add_my_webinar' );
function add_my_webinar( $pages ) {
	 $my_webinar_pages = new WP_Query( array( 'post_type' => 'wpwebinar' ) );
	if ( $my_webinar_pages->post_count > 0 ) {
		$pages = array_merge( $pages, $my_webinar_pages->posts );
	}
	 return $pages;
}

add_action( 'pre_get_posts', 'enable_front_page_webinar' );
function enable_front_page_webinar( $query ) {
	if ( '' == $query->query_vars['post_type'] && 0 != $query->query_vars['page_id'] ) {
		$query->query_vars['post_type'] = array( 'page', 'wpwebinar' );
	}
}

add_action( 'template_redirect', 'template_redirect_frontpage_webinar' );
function template_redirect_frontpage_webinar() {
	if ( is_front_page() && is_singular( 'wpwebinar' ) ) {
		include plugin_dir_path( __FILE__ ) . '/webinar.php';
		die();
	}
}

add_action( 'template_redirect', 'webinar_template_redirect' );
function webinar_template_redirect() {
	global $wp_query;
	if ( $wp_query->query_vars['post_type'] == 'wpwebinar' ) {
		include plugin_dir_path( __FILE__ ) . '/webinar.php';
		die(); }
}


// *********************************************************************************** //
// ADDS PLUGIN MENU AND PAGE ICON
// *********************************************************************************** //

// add_action('admin_head', 'add_wpwebinar_icon');
function add_wpwebinar_icon() {
	global $post_type;
	?>
	<style>
	<?php if ( ( $_GET['post_type'] == 'wpwebinar' ) || ( $post_type == 'wpwebinar' ) ) : ?>
	#icon-edit { background:transparent url('<?php echo WP_PLUGIN_URL . '/wp-webinar/images/wpwebinar32.png'; ?>') no-repeat; }		
	<?php endif; ?>
	#adminmenu #menu-posts-wpwebinar div.wp-menu-image{background:transparent url("<?php echo WP_PLUGIN_URL . '/wp-webinar/images/wpwebinar16.png'; ?>") no-repeat center center;}
	#adminmenu #menu-posts-wpwebinar:hover div.wp-menu-image,#adminmenu #menu-posts-wpwebinar.wp-has-current-submenu div.wp-menu-image{background:transparent url("<?php echo WP_PLUGIN_URL . '/wp-webinar/images/wpwebinar16.png'; ?>") no-repeat center center;}	
</style>
	<?php
}

// *********************************************************************************** //
// ADDS BACKWARD FUNCTIONALITY FOR SLL AND STANDARD VARIABLES
// *********************************************************************************** //

if ( ! function_exists( 'is_ssl' ) ) {
	function is_ssl() {
		if ( isset( $_SERVER['HTTPS'] ) ) {
			if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
				return true;
			}
			if ( '1' == $_SERVER['HTTPS'] ) {
				return true;
			}
		} elseif ( isset( $_SERVER['SERVER_PORT'] ) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
			return true;
		}
		return false;
	}
}

if ( version_compare( get_bloginfo( 'version' ), '3.0', '<' ) && is_ssl() ) {
	$wp_content_url = str_replace( 'http://', 'https://', get_option( 'siteurl' ) );
} else {
	$wp_content_url = get_option( 'siteurl' );
}
 $wp_content_url .= '/wp-content';
 $wp_content_dir  = ABSPATH . 'wp-content';
 $wp_plugin_url   = $wp_content_url . '/plugins';
 $wp_plugin_dir   = $wp_content_dir . '/plugins';
 $wpmu_plugin_url = $wp_content_url . '/mu-plugins';
 $wpmu_plugin_dir = $wp_content_dir . '/mu-plugins';

// *********************************************************************************** //
// ADDS TEMPLATE CHOICE
// *********************************************************************************** //

function wpwebinar_template_redirect() {
	global $wp_query;
	if ( $wp_query->query_vars['post_type'] == 'wpwebinar' ) {
		include plugin_dir_path( __FILE__ ) . '/webinar.php';
		die(); }
}

add_action( 'template_redirect', 'wpwebinar_template_redirect' );


// *********************************************************************************** //
// ADD STYLESHEET TO THEME PAGE
// *********************************************************************************** //

function wpwebinar_enqueue_styles() {
	global $post_type;

	// if(isset($post_type) && $post_type == 'wpwebinar') {
		wp_register_style( 'wpwebinarstylesheet', WP_PLUGIN_URL . '/wp-webinar/webinar-style.css' );
		wp_enqueue_style( 'wpwebinarstylesheet', plugins_url( '/wp-webinar/webinar-style.css', __FILE__ ), false, '1.1', 'all' );
	// }
}
add_action( 'wp_enqueue_scripts', 'wpwebinar_enqueue_styles' );



// Enqueue Javascript
function wpwebinar_enqueue_js() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'countdown', WP_PLUGIN_URL . '/wp-webinar/js/countdown.php' );
	}}

// Actions
add_action( 'init', 'wpwebinar_enqueue_js' );


// *********************************************************************************** //
// ADDS TINYMCE STYLESHEET
// *********************************************************************************** //

add_filter( 'mce_css', 'wpwebinar_editor_style' );
function wpwebinar_editor_style( $url ) {
	global $post_type;

	// if(isset($post_type) && $post_type == 'wpwebinar') {

	if ( ! empty( $url ) ) {
		$url .= ',';
	}
		  // Change the path here if using sub-directory
		  $url .= WP_PLUGIN_URL . '/' . str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) . 'wpwebinar-editor-style.css';

		  return $url;
	// }
}


// *********************************************************************************** //
// ADDS APPENDO FOR DUPLICATING RECURRING TIMES
// *********************************************************************************** //

function add_admin_scripts( $hook ) {

	global $post;

	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		if ( $post->post_type == 'wpwebinar' ) {
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'appendo', WP_PLUGIN_URL . '/wp-webinar/js/jquery.appendo.js' );
			wp_enqueue_script( 'slider-manager', WP_PLUGIN_URL . '/wp-webinar/js/manager.js' );
		}
	}
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );


// *********************************************************************************** //
// ADMIN STYLES
// *********************************************************************************** //

function wpwebinar_editor_styles() {
	global $post;
	global $post_type;
	if ( $post && $post->post_type && $post->post_type == 'wpwebinar' ) {
		echo '
		<style type="text/css" media="screen">
			#content_code, #content_add_image, #content_add_video, #content_add_media, #content_add_audio { display: none !important; } 
			.wpwebinar .description {font-style: normal; font-size: 12px; margin: 0px 0px 15px 0px;}
			.wpwebinar #postcustomstuff table {border: none; background-color: #fff; padding: 4px; margin-bottom: 25px!important;}			 
			.wpwebinar .shorttext {width: 50px !important; text-align: center;}			 
			.wpwebinar input[type="text"] {height: 35px; width: 100%; margin-bottom: 15px;}
			.wp-color-picker {height: 24px !important; width: 60px !important; margin-bottom: 0px !important;}
			.wpwebinar input[type="checkbox"] {margin-bottom: 15px;}
			.wpwebinar label {font-weight: bold; font-size: 13px; margin-bottom: 8px; display: block;}
			.wpwebinar #divid {width: 100%; margin: 0px auto 15px auto; border-bottom: 1px solid #ccc;}
			.wpwebinar {padding: 10px;}
			.wpwebinar #editorcontainer {margin-bottom: 15px;}
			.wpwebinar .cbalign {width: 15px; height:15px; float: left;}
			.wpwebinar .cblabel {padding-left: 3px; float: left; margin: 0px 15px 8px 0px; font-weight: normal; font-size: 12px;}
			.wpwebinar #cbstuff {margin-right: 25px; float: left;}
			.wpwebinar .mceResize {top: 0px !important; margin: 0px !important;}
			.wpwebinar textarea {margin-bottom: 25px !important;}
			.wp-editor-container {background-color: #fff; margin-bottom: 25px;}
			.postbox {background-color: #fff;}
			#theme_image_checkbox input[type=checkbox] { display:none; }
			#theme_image_checkbox input[type=checkbox] + label {background-image: url(\'';

			echo get_template_directory_uri();
			echo '/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox input[type=checkbox]:checked + label {background-image: url(\'';
			echo get_template_directory_uri();
			echo '/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			
			#theme_image_checkbox1 input[type=checkbox] { display:none; }
			#theme_image_checkbox1 input[type=checkbox] + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-webinar/templates/basic/images/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox1 input[type=checkbox]:checked + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-webinar/templates/basic/images/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}


			#theme_image_checkbox2 input[type=checkbox] { display:none; }
			#theme_image_checkbox2 input[type=checkbox] + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-webinar/templates/modern/images/screenshot.png';
			echo '\');filter: grayscale(100%); -webkit-filter: grayscale(100%); -moz-filter: grayscale(100%);background-size: cover;border: 2px solid #ccc;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
			#theme_image_checkbox2 input[type=checkbox]:checked + label {background-image: url(\'';
			echo WP_PLUGIN_URL;
			echo '/wp-webinar/templates/modern/images/screenshot.png';
			echo '\');filter: grayscale(0%); -webkit-filter: grayscale(0%); -moz-filter: grayscale(0%);background-size: cover;border: 4px solid #d90000;height: 100px;width: 150px;display:inline-block;padding: 0 0 0 0px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
		</style>
		';
	}
}
add_action( 'admin_head', 'wpwebinar_editor_styles' );

// *********************************************************************************** //
// UPLOAD IMAGE JS FILE
// *********************************************************************************** //

function pw_load_scripts( $hook ) {
	if ( $hook != 'edit.php' && $hook != 'post.php' && $hook != 'post-new.php' ) {
		return;
	}
	wp_enqueue_script( 'custom-js', plugins_url( 'wp-webinar/js/custom-js.js', dirname( __FILE__ ) ) );
}
add_action( 'admin_enqueue_scripts', 'pw_load_scripts' );

// *********************************************************************************** //
// ADDS JAVASCRIPT FOR AUTORESPONDER SELECTION
// *********************************************************************************** //

function wpwebinar_arscript() {
	global $post;
	if ( $post && $post->post_type && $post->post_type == 'wpwebinar' ) {
		?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#wpwebinar_theme_color').wpColorPicker();
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {

	//	$('#getarcode').change(change_selects);
	
	$('#saveBTN').click(function() {
	
		$("#saveSettings").submit();
	
	  return false;
	});
	
	//	$('#wpwebinar_optin_code').keyup(function() {
		$('#wpwebinar_optin_code').bind('paste', function() {
		 setTimeout(function() {		
		//	alert("UP");
			
			 $fullARcode =  $('#wpwebinar_optin_code').val(); 
			
			$.post("
			<?php
			echo WP_PLUGIN_URL;
			echo '/wp-webinar/';
			?>
			archange.php", { code: ""+$fullARcode+"" },
			 function(data) {
			 $('#wpwebinar_optin_code').val(data); 
				change_selects(); 
			 }); 
			 
	  return false; }, 100);
	});
	
	function change_selects(){
			var tags = ['a','iframe','frame','frameset','script'], reg, val = $('#wpwebinar_optin_code').val(),
				hdn = $('#arcode_hdn_div2'), formurl = $('#wpwebinar_optin_form_url'), hiddenfields = $('#wpwebinar_optin_hidden_field');
			formurl.val('');
			if(jQuery.trim(val) == '')
				return false;
			$('#arcode_hdn_div').html('');
			$('#arcode_hdn_div2').html('');
			for(var i=0;i<5;i++){
				reg = new RegExp('<'+tags[i]+'([^<>+]*[^\/])>.*?</'+tags[i]+'>', "gi");
				val = val.replace(reg,'');
				
				reg = new RegExp('<'+tags[i]+'([^<>+]*)>', "gi");
				val = val.replace(reg,'');
			}
			var tmpval;
			try {
				tmpval = decodeURIComponent(val);
			} catch(err){
				tmpval = val;
			}
			hdn.append(tmpval);
			var num = 0;
			var name_selected = '';
			var email_selected = '';
			var phone_selected = '';
			$(':text',hdn).each(function(){
				var name = $(this).attr('name'),
					name_selected = num == '0' ? name : (num != '0' ? name_selected : ''), 
					email_selected = num == '1' ? name : email_selected;
					phone_selected = num == '2' ? name : phone_selected;
					if(num=='0') jQuery('#wpwebinar_optin_name_field').val(name_selected);
					if(num=='1') jQuery('#wpwebinar_optin_email_field').val(email_selected);
					if(num=='2') jQuery('#ar_phone').val(phone_selected);
			num++;
			});
			jQuery(':input[type=hidden]',hdn).each(function(){
				jQuery('#arcode_hdn_div').append(jQuery('<input type="hidden" name="'+jQuery(this).attr('name')+'" />').val(jQuery(this).val()));
			});		
			var hidden_f = jQuery('#arcode_hdn_div').html();
			formurl.val(jQuery('form',hdn).attr('action'));
			hiddenfields.val(hidden_f);
			hdn.html('');
			
		};


});
</script>

		<?php
	} }
add_action( 'admin_head', 'wpwebinar_arscript' );

// Color Picker For Admin

function webinar_Colorpicker() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker' );
}

add_action( 'admin_enqueue_scripts', 'webinar_Colorpicker' );


// *********************************************************************************** //
// METABOXES
// *********************************************************************************** //

$prefix = 'wpwebinar_';

$meta_boxes_webinar = array(

	array(
		'id'       => 'webinar_theme',
		'title'    => 'Theme Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => '',
				'desc' => 'Select the theme you wish to display for your webinars. You can use your WordPress default them or you can utilize a choice of the themes built into WPWebinar.',
				'id'   => $prefix . 'optinpage_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name' => 'Your Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_yours',
				'type' => 'checkboxtheme',
			),
			array(
				'name' => 'Basic Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_basic',
				'type' => 'checkboxtheme1',
				'std'  => '',
			),
			array(
				'name' => 'Modern Theme',
				'desc' => '',
				'id'   => $prefix . 'theme_modern',
				'type' => 'checkboxtheme2',
				'std'  => '',
			),
			array(
				'name' => 'Theme Color',
				'desc' => 'Select the primary color of your theme. This function is only utilized with the built in WPWebinar themes.',
				'id'   => $prefix . 'theme_color',
				'type' => 'colorpicker',
				'std'  => '#fff',
			),
			array(
				'name' => 'Upload Your Logo',
				'desc' => 'Upload the logo you would like to display on your webinars. This is only utilized with the built in WPWebinar themes. IMPORTANT - When the window pops up, you need to ensure the "Link URL" field is filled in. If not then click the "File URL" button to fill it in. Then click "Insert Into Post".',
				'id'   => $prefix . 'logo',
				'type' => 'image',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'settings_headline_mb',
		'title'    => 'Webinar Optin Page Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => '',
				'desc' => 'Some themes will require you to set the width of the optin page area as well as the top margin',
				'id'   => $prefix . 'optinpage_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name'    => 'Top Margin',
				'desc'    => 'Sets top margin of webinar page.',
				'id'      => $prefix . 'top_margin',
				'type'    => 'shortselect',
				'options' => array( '', '5px', '10px', '15px', '20px', '25px', '30px', '35px', '40px', '45px', '50px', '55px', '60px', '65px', '70px', '75px', '80px', '85px', '90px', '95px', '100px' ),
			),
			array(
				'name'    => 'Sales Letter Width',
				'desc'    => 'Sets width of webinar page.',
				'id'      => $prefix . 'wpwebinar_width',
				'type'    => 'shortselect2',
				'options' => array( '', '800px', '810px', '820px', '830px', '840px', '850px', '860px', '870px', '880px', '890px', '900px', '910px', '920px', '930px', '940px', '950px', '960px', '970px', '980px', '990px', '1000px' ),
			),
		),
	),

	array(
		'id'       => 'main_headline_mb',
		'title'    => 'Webinar Optin Headline',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Webinar Optin Headline',
				'desc' => 'Enter the main headline to appear at the top of your optin page. Use the Kitchen Sink to style it and toolbar to center.',
				'id'   => $prefix . 'main_headline',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'optin_media',
		'title'    => 'Media Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Media Settings',
				'desc' => 'Select to display a video or audio on your optin page. You video must be in an MP4 format to work properly.',
				'id'   => $prefix . 'media_settings_text',
				'type' => 'plaintext',
			),
			array(
				'name' => 'Built-In Video Player',
				'desc' => '',
				'id'   => $prefix . 'show_video',
				'type' => 'checkboxvideo',
				'std'  => '',
			),
			array(
				'name' => 'Built-In Audio Player',
				'desc' => '',
				'id'   => $prefix . 'show_audio',
				'type' => 'checkboxaudio',
				'std'  => '',
			),
			array(
				'name' => 'Your Own Video Player',
				'desc' => '',
				'id'   => $prefix . 'show_videobox',
				'type' => 'checkboxvideobox',
				'std'  => '',
			),
			array(
				'name' => 'Video URL',
				'desc' => 'Enter the location of the URL of your MP4 video below.',
				'id'   => $prefix . 'video_url',
				'type' => 'hiddenvideo1',
				'std'  => '',
			),
			array(
				'name' => 'Video Width',
				'desc' => 'Enter the width of your video. We recommend 600px wide or if you have a wide theme you may go up to 640px this can always change. Just enter a number, do not enter px after the number.',
				'id'   => $prefix . 'video_width',
				'type' => 'hiddenvideo2',
				'std'  => '',
			),
			array(
				'name' => 'Video Height',
				'desc' => 'Your video height will depend on where you did a 4x3 or 16x9 resolution of your video. Enter the number below.',
				'id'   => $prefix . 'video_height',
				'type' => 'hiddenvideo3',
				'std'  => '',
			),
			array(
				'name' => 'Audio MP3 URL',
				'desc' => 'Enter the URL of your MP3 file.',
				'id'   => $prefix . 'audio_url',
				'type' => 'hiddenaudio',
				'std'  => '',
			),
			array(
				'name' => 'Your Own Video Player',
				'desc' => 'Paste your video code into the box below.',
				'id'   => $prefix . 'videobox_code',
				'type' => 'hiddenvideobox',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'optin_box_settings_mb',
		'title'    => 'Optin Box Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optin Box Headline',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'box_headline',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Optin Box Description',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'box_description',
				'type' => 'text',
				'std'  => '',
			),
		),
	),

	array(
		'id'       => 'autoresponder_settings_mb',
		'title'    => 'Autoresponder Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Paste In Your Autoresponder Settings',
				'desc' => 'WPWebinar works with any autoresponder system. Simply make sure your optin code has BOTH a name field and an email field. The name field must be first in the code. Most autoresponders work like this by default. Paste the code below. When successful you will see the 3 fields filled in with the correct field parameters.',
				'id'   => $prefix . 'optin_code',
				'type' => 'textarea',
			),
			array(
				'name' => 'Name Field',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_name_field',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Email Field',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_email_field',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Form URL',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_form_url',
				'type' => 'texthidden',
			),
			array(
				'name' => 'Hidden Fields',
				'desc' => 'Choose your autoresponder service or software below. If your service is NOT shown, choose other to paste in you autoresponder code.',
				'id'   => $prefix . 'optin_hidden_field',
				'type' => 'hiddentextarea',
			),
		),
	),

	array(
		'id'       => 'optin_button_settings_mb',
		'title'    => 'Optin Button Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optin Button Text',
				'desc' => 'The headline above the optin box. Keep it short and to the point.',
				'id'   => $prefix . 'button_text',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Choose Optin Buttons',
				'desc' => 'Select which button you would like to use.',
				'id'   => $prefix . 'button_orange',
				'type' => 'checkboxorange',
				'std'  => '',
			),
			array(
				'name' => '',
				'desc' => '',
				'id'   => $prefix . 'button_green',
				'type' => 'checkboxgreen',
				'std'  => '',
			),
			array(
				'name' => '',
				'desc' => '',
				'id'   => $prefix . 'button_blue',
				'type' => 'checkboxblue',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'optional_text_mb',
		'title'    => 'Optional Text Under Video',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Optional Text Below Video',
				'desc' => 'This is an optional entry. If you choose, you can type text below the video with additional information. You may also insert images here through the media uploader.',
				'id'   => $prefix . 'optional_text',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'footertext_mb',
		'title'    => 'Optin Footer Text',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Footer Text ',
				'desc' => 'This text is at the bottom of the page to insert legal information or other information you would like to display.',
				'id'   => $prefix . 'footer_text',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'thankyou_page_text_mb',
		'title'    => 'Thank You Page Content',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Thank You Video URL',
				'desc' => 'Enter the URL for a video to be displayed on your thank you page.',
				'id'   => $prefix . 'thankyou_page_video_url',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Video Width',
				'desc' => 'Enter the width of the video below.',
				'id'   => $prefix . 'thankyou_page_video_width',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Video Height',
				'desc' => 'Enter the height of the video below.',
				'id'   => $prefix . 'thankyou_page_video_height',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Thank You Page Content',
				'desc' => 'Enter any additional information you want on the thank you page. This will appear under the video.',
				'id'   => $prefix . 'thankyou_page_content',
				'type' => 'wysiwyg',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'webinar_settings_mb',
		'title'    => 'Webinar Settings',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Choose Webinar Timezone',
				'id'   => $prefix . 'webinar_time_settings_text',
				'desc' => 'You can choose between local and admin time. Local time will always play the webinar in the viewers local time zone. Admin time will force viewers to watch in your timezone. We highly advise you choose Local Time as it will garner the most people for your webinars.',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'Local Time',
				'desc' => '',
				'id'   => $prefix . 'localtime',
				'type' => 'checkboxlocaltime',
				'std'  => '',
			),

			array(
				'name' => 'Admin Time',
				'desc' => '',
				'id'   => $prefix . 'admintime',
				'type' => 'checkboxadmintime',
				'std'  => '',
			),
			array(
				'name' => 'Select Your Timezone',
				'desc' => 'Choose your timezone from the dropdown menu below.',
				'id'   => $prefix . 'webinar_timezone',
				'type' => 'timezoneselect',
				'std'  => '',
			),
			array(
				'name' => 'Select Webinar Occurance, Days and Times',
				'id'   => $prefix . 'webinar_settings_text',
				'desc' => '',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'One Time Webinar',
				'desc' => '',
				'id'   => $prefix . 'onetime_webinar',
				'type' => 'checkboxwebinar',
				'std'  => '',
			),

			array(
				'name' => 'Recurring Webinar',
				'desc' => '',
				'id'   => $prefix . 'recurring_webinar',
				'type' => 'checkboxwebinars',
				'std'  => '',
			),
			array(
				'name' => 'One Time Webinar',
				'type' => 'onetimewebinartext',
				'id'   => $prefix . 'onetime_settings_text',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Date:',
				'desc' => '',
				'id'   => $prefix . 'webinar_month',
				'type' => 'monthselect',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Date:',
				'desc' => '',
				'id'   => $prefix . 'webinar_day',
				'type' => 'dayselect',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Date:',
				'desc' => '',
				'id'   => $prefix . 'webinar_year',
				'type' => 'yearselect',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Time: ',
				'desc' => '',
				'id'   => $prefix . 'webinar_hour',
				'type' => 'timeselect2',
				'std'  => '',
			),
			array(
				'name' => 'Choose Days of The Week',
				'desc' => 'Check which days of the week you want to play your webinar.',
				'id'   => $prefix . 'dayofweek_text',
				'type' => 'plaintextrecurring',
				'std'  => '',
			),
			array(
				'name' => 'Sunday',
				'desc' => '',
				'id'   => $prefix . 'sunday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Monday',
				'desc' => '',
				'id'   => $prefix . 'monday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Tuesday',
				'desc' => '',
				'id'   => $prefix . 'tuesday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Wednesday',
				'desc' => '',
				'id'   => $prefix . 'wednesday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Thursday',
				'desc' => '',
				'id'   => $prefix . 'thursday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Friday',
				'desc' => '',
				'id'   => $prefix . 'friday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name' => 'Saturday',
				'desc' => '',
				'id'   => $prefix . 'saturday',
				'type' => 'checkboxtime',
				'std'  => '',
			),
			array(
				'name'    => 'Recurring Webinar Time',
				'desc'    => '',
				'id'      => $prefix . 'recurring_time',
				'type'    => 'recurring_select_time',
				'std'     => '',
				'options' => array(
					'1'  => '01:00 AM',
					'2'  => '02:00 AM',
					'3'  => '03:00 AM',
					'4'  => '04:00 AM',
					'5'  => '05:00 AM',
					'6'  => '06:00 AM',
					'7'  => '07:00 AM',
					'8'  => '08:00 AM',
					'9'  => '09:00 AM',
					'10' => '10:00 AM',
					'11' => '11:00 AM',
					'12' => '12:00 PM',
					'13' => '01:00 PM',
					'14' => '02:00 PM',
					'15' => '03:00 PM',
					'16' => '04:00 PM',
					'17' => '05:00 PM',
					'18' => '06:00 PM',
					'19' => '07:00 PM',
					'20' => '08:00 PM',
					'21' => '09:00 PM',
					'22' => '10:00 PM',
					'23' => '11:00 PM',
					'24' => '12:00 AM',
				),

			),
			array(
				'name' => 'Webinar Video URL',
				'desc' => 'Enter the URL for your recorded webinar video.',
				'id'   => $prefix . 'webinar_video_url',
				'type' => 'webinarvideotext',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Video Width',
				'desc' => 'Enter the width of your webinar video.',
				'id'   => $prefix . 'webinar_video_width',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Webinar Video Height',
				'desc' => 'Enter the height of your webinar video.',
				'id'   => $prefix . 'webinar_video_height',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Length of Webinar',
				'id'   => $prefix . 'webinar_length_text',
				'desc' => 'In the fields below, enter the number of minutes and seconds of how long your webinar is.',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'Minutes',
				'desc' => 'Minutes into video.',
				'id'   => $prefix . 'webinar_minutes',
				'type' => 'minutes',
				'std'  => '0',
			),
			array(
				'name' => 'Seconds',
				'desc' => 'Seconds into video.',
				'id'   => $prefix . 'webinar_seconds',
				'type' => 'seconds',
				'std'  => '0',
			),
		),
	),
	array(
		'id'       => 'call_to_action_area',
		'title'    => 'Call To Action Area',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Call To Action Display Time',
				'id'   => $prefix . 'cta_display_time',
				'desc' => 'In the fields below, enter the number of minutes and seconds before your call to action is displayed.',
				'type' => 'plaintext',
				'std'  => '',
			),
			array(
				'name' => 'Minutes',
				'desc' => 'Minutes into video.',
				'id'   => $prefix . 'minutes',
				'type' => 'minutes',
				'std'  => '0',
			),
			array(
				'name' => 'Seconds',
				'desc' => 'Seconds into video.',
				'id'   => $prefix . 'seconds',
				'type' => 'seconds',
				'std'  => '0',
			),
			array(
				'name' => 'Call To Action URL',
				'desc' => 'Enter URL you would like the prospects to go to.',
				'id'   => $prefix . 'cta_url',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Call To Action Button Text',
				'desc' => 'Enter text you would like on the call to action button.',
				'id'   => $prefix . 'cta_button_text',
				'type' => 'text',
				'std'  => '',
			),
		),
	),
	array(
		'id'       => 'webinar_emails_mb',
		'title'    => 'Webinar Email Notifications',
		'page'     => 'wpwebinar',
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => 'Set the From Email Address For Your Email Messages',
				'desc' => 'Set the from email for the messages. Note: If possible I suggest you use an email on the domain which this webinar will be played from.',
				'id'   => $prefix . 'from_email',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Set the From Name From Which Your Message Will Come From',
				'desc' => 'What name will your customers recognize as the email being from..',
				'id'   => $prefix . 'from_name',
				'type' => 'text',
				'std'  => '',
			),
			array(
				'name' => 'Email One Subject',
				'desc' => 'This is the initial email that is sent out immediately after registration.',
				'id'   => $prefix . 'email_one_subject',
				'type' => 'text',
				'std'  => 'Your Webinar Details',
			),
			array(
				'name' => 'Email One Message',
				'desc' => 'Type in the reminder message sent to your attendees immediately after registration.',
				'id'   => $prefix . 'email_one_message',
				'type' => 'textarea',
				'std'  => "From: INSERT YOUR NAME HERE \nRe: Your Webinar Details \n\nThank you for registering for our webinar. \nBelow is below is the time and date of your webinar, be sure to keep it in a safe place. \n\nDATE: %WEBINARDATE% \nTIME: %WEBINARTIME% \nURL: %WEBINARLINK% \n\nI look forward to seeing you on the webinar. \n\nBest of Success, \nINSERT YOUR NAME \n \n \n\n",
			),
			array(
				'name' => 'Email Reminder 24 Hours Before Subject',
				'desc' => 'This is the email that is sent out 24 hours before webinar.',
				'id'   => $prefix . 'email_24hour_subject',
				'type' => 'text',
				'std'  => 'Reminder... Your Webinar Starts in 24 Hours',
			),
			array(
				'name' => 'Email Reminder 24 Hours Before Message',
				'desc' => 'Type in the reminder message sent to your attendees 24 hours before webinar.',
				'id'   => $prefix . 'email_24hour_message',
				'type' => 'textarea',
				'std'  => "From: INSERT YOUR NAME HERE \nRe: Reminder... The Webinar Starts in 24 Hours \n\nI just wanted to send you a reminder about our webinar tomorrow. \nBe sure you are there on time, so you don't miss anything. \n\nDATE: %WEBINARDATE% \nTIME: %WEBINARTIME% \nURL: %WEBINARLINK% \n\nI look forward to seeing you on the webinar. \n\nBest of Success, \nINSERT YOUR NAME HERE \n \n \n\n",
			),
			array(
				'name' => 'Email Reminder 1 Hours Before Subject',
				'desc' => 'This is the email that is sent out 1 hour before webinar.',
				'id'   => $prefix . 'email_1hour_subject',
				'type' => 'text',
				'std'  => 'Reminder... Your Webinar Starts in 1 Hour',
			),
			array(
				'name' => 'Email Reminder 1 Hour Before Message',
				'desc' => 'Type in the reminder message sent to your attendees 1 before the webinar.',
				'id'   => $prefix . 'email_1hour_message',
				'type' => 'textarea',
				'std'  => "From: INSERT YOUR NAME HERE \nRe: Reminder... Your Webinar Starts in 1 Hour \n\nIn case you forgot, I just wanted to remind you the webinar starts in 1 hour. \nBelow is your link to the webinar page. If you get there early, you will see a coutndown timer. When the countdown reaches zero the webinar will start automatically. \n\nDATE: %WEBINARDATE% \nTIME: %WEBINARTIME% \nURL: %WEBINARLINK% \n\nI look forward to seeing you on the webinar. \n\nBest of Success, \nINSERT YOUR NAME HERE \n \n \n\n",
			),
			array(
				'name' => 'Replay Email Subject',
				'desc' => 'This is the email that is sent out the day after the webinar.',
				'id'   => $prefix . 'email_replay_subject',
				'type' => 'text',
				'std'  => "Did You Miss It? There's Good News!",
			),
			array(
				'name' => 'Replay Email Message',
				'desc' => 'Type in the reminder message sent to your attendees the day after webinar.',
				'id'   => $prefix . 'email_replay_message',
				'type' => 'textarea',
				'std'  => "From: INSERT YOUR NAME HERE \nRe: Did You Miss It? There's Good News! \n\nIf you missed the webinar yesterday, you're in luck. I'm have the recording up and it's waiting for you now. \nIt won't but up for long, so click the link below and watch soon. \n\nHere's the link: %WEBINARLINK% \n\nIf you have any questions let me know. \n\nBest of Success, \nINSERT YOUR NAME HERE \n\n\n\n",
			),
		),
	),
);


// *********************************************************************************** //
// REGISTERS THE METABOX
// *********************************************************************************** //

add_action( 'add_meta_boxes', 'wpwebinar_add_box' );

function wpwebinar_add_box() {
	global $meta_boxes_webinar;

	foreach ( $meta_boxes_webinar as $meta_box_webinar ) :
		add_meta_box( $meta_box_webinar['id'], $meta_box_webinar['title'], 'wpwebinar_show_box', $meta_box_webinar['page'], $meta_box_webinar['context'], $meta_box_webinar['priority'], array( 'id' => $meta_box_webinar['id'] ) );
	endforeach;
}

// *********************************************************************************** //
// METABOX CALLBACK AND DISPLAY
// *********************************************************************************** //

function wpwebinar_show_box( $post, $mb_id ) {
	global $meta_boxes_webinar, $post, $post_id;
	wp_nonce_field( plugin_basename( __FILE__ ), 'wpwebinar_meta_box_nonce' );
	wp_create_nonce( 'wpwebinar_meta_box_nonce' );

	// Use nonce for verification
	echo '<input type="hidden" id="wpwebinar_meta_box_nonce" name="wpwebinar_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />' . "\n";

	echo '<div class="wpwebinar">' . "\n";

	foreach ( $meta_boxes_webinar as $meta_box_webinar ) {
		if ( $meta_box_webinar['id'] == $mb_id['id'] ) :
			foreach ( $meta_box_webinar['fields'] as $field ) {
				// get current post meta data

				$meta = get_post_meta( $post->ID, $field['id'], true );

				switch ( $field['type'] ) {
					case 'image':
						$image = WP_PLUGIN_URL . '/wp-webinar/images/default.png';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<span class="custom_default_image" style="display:none">' . $image . '</span>';
						if ( $meta ) {
							$image = wp_get_attachment_image_src( $meta, 'medium' );
							$image = $image[0]; }
						echo '<input name="' . $field['id'] . '" type="hidden" class="custom_upload_image" value="' . $meta . '" />
									<img src="' . $image . '" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small><a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" />';
						break;

					case 'text':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" />' . "\n";
						break;

					case 'colorpicker':
						echo '<div id="image_uploader" style="clear: both; margin-bottom: 15px;"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" data-default-color="#ffffff" /></div>' . "\n";
						break;

					case 'texthidden':
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:100%">' . "\n";
						break;

					case 'textarea':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="10" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						break;

					case 'hiddentextarea':
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:100%; display:none">', $meta ? $meta : $field['std'], '</textarea><div id="arcode_hdn_div" style="display: none;"></div><div id="arcode_hdn_div2" style="display: none;"></div>';
						break;

					case 'minutes':
						echo '<input type="text" class="shorttext" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" /><strong> : </strong>';
						break;

					case 'seconds':
						echo '<input type="text" class="shorttext" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" />' . "\n";
						break;

					case 'webinarvideotext':
						echo '</div><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" />' . "\n";
						break;

					case 'onetimewebinartext':
						echo '<div id="onetime_box" style="display:none; border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;"><label for="', $field['id'], '">', $field['name'], '</label>' . "\n";
						break;

					case 'testtext':
						echo '<table class="appendo"><tr><td><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" /></tr></td></table>' . "\n";
						break;

					case 'plaintext':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						break;

					case 'plaintextrecurring':
						echo '<div id="recurring_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						break;

					// HIDDEN VIDEO AUDIO CASE

					case 'hiddenvideo1':
						echo '<div id="video_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						break;

					case 'hiddenvideo2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						break;

					case 'hiddenvideo3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>';
						break;

					case 'hiddenaudio':
						echo '<div id="audio_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">' . "\n";
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>' . "\n";
						break;

					case 'hiddenvideobox':
						echo '<div id="videobox" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="10" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						echo '</div>';
						break;

					// HIDDEN AWEBER

					case 'hiddenaweber1':
						echo '<div id="aweber_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">' . "\n";
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />' . "\n";
						echo '</div>';
						break;

					case 'hiddenaweber2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN 1SHOPPINGCART

					case 'hidden1shoppingcart1':
						echo '<div id="1shoppingcart_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hidden1shoppingcart2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hidden1shoppingcart3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN GETRESPONSE

					case 'hiddengetresponse1':
						echo '<div id="getresponse_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					case 'hiddengetresponse2':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddengetresponse3':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN ARP3

					case 'hiddenarp31':
						echo '<div id="arp3_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddenarp32':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						break;

					case 'hiddenarp33':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />';
						echo '</div>';
						break;

					// HIDDEN OTHER

					case 'hiddenother':
						echo '<div id="other_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						echo '</div>';
						break;

					case 'textarea':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="8" style="width:100%">', $meta ? $meta : $field['std'], '</textarea>';
						break;

					case 'wysiwyg':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						$settings = array( 'textarea_rows' => '3' );
						wp_editor( $meta ? $meta : $field['std'], $field['id'], $settings );
						break;

					case 'select':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						break;

					case 'selecttestoriginal':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $value => $option ) {
							echo '<option value="',$value,'"', $meta == $value ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						break;

					// TIME SELECT TEST2 $meta = get_post_meta($post->ID, $field['id'], true);
					case 'recurring_select_time':
						global $post;
						$post_id = $post->ID;

						echo '<div style="clear:both;"><label for="', $field['id'], '">', $field['name'], '</label></div><div class="description">', $field['desc'],'</div>' . "\n";

						echo '<ul id="tester_wrap">' . "\n";

						if ( get_post_meta( $post_id, 'wpwebinar_recurring_time', true ) ) :

									$a = get_post_meta( $post->ID, 'wpwebinar_recurring_time', true );
							foreach ( $a as $k => $v ) {
								echo '<li class="tester">' . "\n";

								echo '<select name="wpwebinar_recurring_time[]">' . "\n";
								foreach ( $field['options'] as $value => $option ) {
									echo '<option	value= "' . $value . '" ' . ( ( $v == $value ) ? 'selected="selected"' : '' ) . '>	' . $option . '</option>	' . $v . '' . "\n";
								}
								echo '</select><button class="remove_slide button-secondary">Remove</button><div style="clear:both"></div>' . "\n";

								echo '</li>' . "\n";
							}

							else :

								echo '<li class="tester">';

									echo '<select name="wpwebinar_recurring_time[]">';
								foreach ( $field['options'] as $value => $option ) {
									echo '<option value="',$value,'">', $option, '</option>';
								}
									echo '</select>';

								echo '</li>';

							endif;

							echo '</ul>' . "\n";
						break;

					// TIME SELECT TEST CURRENT

					case 'timeselect':
						echo '<div style="clear: both"></div><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						global $post;
						$post_id = $post->ID;

						$webinartime = get_post_meta( $post_id, 'wpwebinar_webinar_time', true );

						echo '<table class="appendo"><tr><td><select name="wpwebinar_webinar_time[]" id="wpwebinar_webinar_time[]">';
						echo '<option value="">Select...</option>' . "\n";
						echo '<option value="1"' . ( ( $webinartime == '1' ) ? 'selected="selected"' : '' ) . '>01:00 AM</option>' . "\n";
						echo '<option value="2"' . ( ( $webinartime == '2' ) ? 'selected="selected"' : '' ) . '>02:00 AM</option>' . "\n";
						echo '<option value="3"' . ( ( $webinartime == '3' ) ? 'selected="selected"' : '' ) . '>03:00 AM</option>' . "\n";
						echo '<option value="4"' . ( ( $webinartime == '4' ) ? 'selected="selected"' : '' ) . '>04:00 AM</option>' . "\n";
						echo '<option value="5"' . ( ( $webinartime == '5' ) ? 'selected="selected"' : '' ) . '>05:00 AM</option>' . "\n";
						echo '<option value="6"' . ( ( $webinartime == '6' ) ? 'selected="selected"' : '' ) . '>06:00 AM</option>' . "\n";
						echo '<option value="7"' . ( ( $webinartime == '7' ) ? 'selected="selected"' : '' ) . '>07:00 AM</option>' . "\n";
						echo '<option value="8"' . ( ( $webinartime == '8' ) ? 'selected="selected"' : '' ) . '>08:00 AM</option>' . "\n";
						echo '<option value="9"' . ( ( $webinartime == '9' ) ? 'selected="selected"' : '' ) . '>09:00 AM</option>' . "\n";
						echo '<option value="10"' . ( ( $webinartime == '10' ) ? 'selected="selected"' : '' ) . '>10:00 AM</option>' . "\n";
						echo '<option value="11"' . ( ( $webinartime == '11' ) ? 'selected="selected"' : '' ) . '>11:00 AM</option>' . "\n";
						echo '<option value="12"' . ( ( $webinartime == '12' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '<option value="13"' . ( ( $webinartime == '13' ) ? 'selected="selected"' : '' ) . '>01:00 PM</option>' . "\n";
						echo '<option value="14"' . ( ( $webinartime == '14' ) ? 'selected="selected"' : '' ) . '>02:00 PM</option>' . "\n";
						echo '<option value="15"' . ( ( $webinartime == '15' ) ? 'selected="selected"' : '' ) . '>03:00 PM</option>' . "\n";
						echo '<option value="16"' . ( ( $webinartime == '16' ) ? 'selected="selected"' : '' ) . '>04:00 PM</option>' . "\n";
						echo '<option value="17"' . ( ( $webinartime == '17' ) ? 'selected="selected"' : '' ) . '>05:00 PM</option>' . "\n";
						echo '<option value="18"' . ( ( $webinartime == '18' ) ? 'selected="selected"' : '' ) . '>06:00 PM</option>' . "\n";
						echo '<option value="19"' . ( ( $webinartime == '19' ) ? 'selected="selected"' : '' ) . '>07:00 PM</option>' . "\n";
						echo '<option value="20"' . ( ( $webinartime == '20' ) ? 'selected="selected"' : '' ) . '>08:00 PM</option>' . "\n";
						echo '<option value="21"' . ( ( $webinartime == '21' ) ? 'selected="selected"' : '' ) . '>09:00 PM</option>' . "\n";
						echo '<option value="22"' . ( ( $webinartime == '22' ) ? 'selected="selected"' : '' ) . '>10:00 PM</option>' . "\n";
						echo '<option value="23"' . ( ( $webinartime == '23' ) ? 'selected="selected"' : '' ) . '>11:00 PM</option>' . "\n";
						echo '<option value="24"' . ( ( $webinartime == '24' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '</select></td></tr></table>';
						break;

					case 'timeselect2':
						echo $field['name'];
						global $post;
						$post_id      = $post->ID;
						$webinartime2 = get_post_meta( $post_id, 'wpwebinar_webinar_hour', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="">Select...</option>' . "\n";
						echo '<option value="1"' . ( ( $webinartime2 == '1' ) ? 'selected="selected"' : '' ) . '>01:00 AM</option>' . "\n";
						echo '<option value="2"' . ( ( $webinartime2 == '2' ) ? 'selected="selected"' : '' ) . '>02:00 AM</option>' . "\n";
						echo '<option value="3"' . ( ( $webinartime2 == '3' ) ? 'selected="selected"' : '' ) . '>03:00 AM</option>' . "\n";
						echo '<option value="4"' . ( ( $webinartime2 == '4' ) ? 'selected="selected"' : '' ) . '>04:00 AM</option>' . "\n";
						echo '<option value="5"' . ( ( $webinartime2 == '5' ) ? 'selected="selected"' : '' ) . '>05:00 AM</option>' . "\n";
						echo '<option value="6"' . ( ( $webinartime2 == '6' ) ? 'selected="selected"' : '' ) . '>06:00 AM</option>' . "\n";
						echo '<option value="7"' . ( ( $webinartime2 == '7' ) ? 'selected="selected"' : '' ) . '>07:00 AM</option>' . "\n";
						echo '<option value="8"' . ( ( $webinartime2 == '8' ) ? 'selected="selected"' : '' ) . '>08:00 AM</option>' . "\n";
						echo '<option value="9"' . ( ( $webinartime2 == '9' ) ? 'selected="selected"' : '' ) . '>09:00 AM</option>' . "\n";
						echo '<option value="10"' . ( ( $webinartime2 == '10' ) ? 'selected="selected"' : '' ) . '>10:00 AM</option>' . "\n";
						echo '<option value="11"' . ( ( $webinartime2 == '11' ) ? 'selected="selected"' : '' ) . '>11:00 AM</option>' . "\n";
						echo '<option value="12"' . ( ( $webinartime2 == '12' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '<option value="13"' . ( ( $webinartime2 == '13' ) ? 'selected="selected"' : '' ) . '>01:00 PM</option>' . "\n";
						echo '<option value="14"' . ( ( $webinartime2 == '14' ) ? 'selected="selected"' : '' ) . '>02:00 PM</option>' . "\n";
						echo '<option value="15"' . ( ( $webinartime2 == '15' ) ? 'selected="selected"' : '' ) . '>03:00 PM</option>' . "\n";
						echo '<option value="16"' . ( ( $webinartime2 == '16' ) ? 'selected="selected"' : '' ) . '>04:00 PM</option>' . "\n";
						echo '<option value="17"' . ( ( $webinartime2 == '17' ) ? 'selected="selected"' : '' ) . '>05:00 PM</option>' . "\n";
						echo '<option value="18"' . ( ( $webinartime2 == '18' ) ? 'selected="selected"' : '' ) . '>06:00 PM</option>' . "\n";
						echo '<option value="19"' . ( ( $webinartime2 == '19' ) ? 'selected="selected"' : '' ) . '>07:00 PM</option>' . "\n";
						echo '<option value="20"' . ( ( $webinartime2 == '20' ) ? 'selected="selected"' : '' ) . '>08:00 PM</option>' . "\n";
						echo '<option value="21"' . ( ( $webinartime2 == '21' ) ? 'selected="selected"' : '' ) . '>09:00 PM</option>' . "\n";
						echo '<option value="22"' . ( ( $webinartime2 == '22' ) ? 'selected="selected"' : '' ) . '>10:00 PM</option>' . "\n";
						echo '<option value="23"' . ( ( $webinartime2 == '23' ) ? 'selected="selected"' : '' ) . '>11:00 PM</option>' . "\n";
						echo '<option value="24"' . ( ( $webinartime2 == '24' ) ? 'selected="selected"' : '' ) . '>12:00 PM</option>' . "\n";
						echo '</select>';
						echo '</div>';
						break;

					case 'monthselect':
						echo $field['name'];
						global $post;
						$post_id      = $post->ID;
						$webinarmonth = get_post_meta( $post_id, 'wpwebinar_webinar_month', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="1"' . ( ( $webinarmonth == '1' ) ? 'selected="selected"' : '' ) . '>January</option>' . "\n";
						echo '<option value="2"' . ( ( $webinarmonth == '2' ) ? 'selected="selected"' : '' ) . '>February</option>' . "\n";
						echo '<option value="3"' . ( ( $webinarmonth == '3' ) ? 'selected="selected"' : '' ) . '>March</option>' . "\n";
						echo '<option value="4"' . ( ( $webinarmonth == '4' ) ? 'selected="selected"' : '' ) . '>April</option>' . "\n";
						echo '<option value="5"' . ( ( $webinarmonth == '5' ) ? 'selected="selected"' : '' ) . '>May</option>' . "\n";
						echo '<option value="6"' . ( ( $webinarmonth == '6' ) ? 'selected="selected"' : '' ) . '>June</option>' . "\n";
						echo '<option value="7"' . ( ( $webinarmonth == '7' ) ? 'selected="selected"' : '' ) . '>July</option>' . "\n";
						echo '<option value="8"' . ( ( $webinarmonth == '8' ) ? 'selected="selected"' : '' ) . '>August</option>' . "\n";
						echo '<option value="9"' . ( ( $webinarmonth == '9' ) ? 'selected="selected"' : '' ) . '>September</option>' . "\n";
						echo '<option value="10"' . ( ( $webinarmonth == '10' ) ? 'selected="selected"' : '' ) . '>October</option>' . "\n";
						echo '<option value="11"' . ( ( $webinarmonth == '11' ) ? 'selected="selected"' : '' ) . '>November</option>' . "\n";
						echo '<option value="12"' . ( ( $webinarmonth == '12' ) ? 'selected="selected"' : '' ) . '>December</option>' . "\n";
						echo '</select>';
						break;

					case 'dayselect':
						global $post;
						$post_id    = $post->ID;
						$webinarday = get_post_meta( $post_id, 'wpwebinar_webinar_day', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option  value="1"' . ( ( $webinarday == '1' ) ? 'selected="selected"' : '' ) . '>1</option>' . "\n";
						echo '<option  value="2"' . ( ( $webinarday == '2' ) ? 'selected="selected"' : '' ) . '>2</option>' . "\n";
						echo '<option  value="3"' . ( ( $webinarday == '3' ) ? 'selected="selected"' : '' ) . '>3</option>' . "\n";
						echo '<option  value="4"' . ( ( $webinarday == '4' ) ? 'selected="selected"' : '' ) . '>4</option>' . "\n";
						echo '<option  value="5"' . ( ( $webinarday == '5' ) ? 'selected="selected"' : '' ) . '>5</option>' . "\n";
						echo '<option  value="6"' . ( ( $webinarday == '6' ) ? 'selected="selected"' : '' ) . '>6</option>' . "\n";
						echo '<option  value="7"' . ( ( $webinarday == '7' ) ? 'selected="selected"' : '' ) . '>7</option>' . "\n";
						echo '<option  value="8"' . ( ( $webinarday == '8' ) ? 'selected="selected"' : '' ) . '>8</option>' . "\n";
						echo '<option  value="9"' . ( ( $webinarday == '9' ) ? 'selected="selected"' : '' ) . '>9</option>' . "\n";
						echo '<option  value="10"' . ( ( $webinarday == '10' ) ? 'selected="selected"' : '' ) . '>10</option>' . "\n";
						echo '<option  value="11"' . ( ( $webinarday == '11' ) ? 'selected="selected"' : '' ) . '>11</option>' . "\n";
						echo '<option  value="12"' . ( ( $webinarday == '12' ) ? 'selected="selected"' : '' ) . '>12</option>' . "\n";
						echo '<option  value="13"' . ( ( $webinarday == '13' ) ? 'selected="selected"' : '' ) . '>13</option>' . "\n";
						echo '<option  value="14"' . ( ( $webinarday == '14' ) ? 'selected="selected"' : '' ) . '>14</option>' . "\n";
						echo '<option  value="15"' . ( ( $webinarday == '15' ) ? 'selected="selected"' : '' ) . '>15</option>' . "\n";
						echo '<option  value="16"' . ( ( $webinarday == '16' ) ? 'selected="selected"' : '' ) . '>16</option>' . "\n";
						echo '<option  value="17"' . ( ( $webinarday == '17' ) ? 'selected="selected"' : '' ) . '>17</option>' . "\n";
						echo '<option  value="18"' . ( ( $webinarday == '18' ) ? 'selected="selected"' : '' ) . '>18</option>' . "\n";
						echo '<option  value="19"' . ( ( $webinarday == '19' ) ? 'selected="selected"' : '' ) . '>19</option>' . "\n";
						echo '<option  value="20"' . ( ( $webinarday == '20' ) ? 'selected="selected"' : '' ) . '>20</option>' . "\n";
						echo '<option  value="21"' . ( ( $webinarday == '21' ) ? 'selected="selected"' : '' ) . '>21</option>' . "\n";
						echo '<option  value="22"' . ( ( $webinarday == '22' ) ? 'selected="selected"' : '' ) . '>22</option>' . "\n";
						echo '<option  value="23"' . ( ( $webinarday == '23' ) ? 'selected="selected"' : '' ) . '>23</option>' . "\n";
						echo '<option  value="24"' . ( ( $webinarday == '24' ) ? 'selected="selected"' : '' ) . '>24</option>' . "\n";
						echo '<option  value="25"' . ( ( $webinarday == '25' ) ? 'selected="selected"' : '' ) . '>25</option>' . "\n";
						echo '<option  value="26"' . ( ( $webinarday == '26' ) ? 'selected="selected"' : '' ) . '>26</option>' . "\n";
						echo '<option  value="27"' . ( ( $webinarday == '27' ) ? 'selected="selected"' : '' ) . '>27</option>' . "\n";
						echo '<option  value="28"' . ( ( $webinarday == '28' ) ? 'selected="selected"' : '' ) . '>28</option>' . "\n";
						echo '<option  value="29"' . ( ( $webinarday == '29' ) ? 'selected="selected"' : '' ) . '>29</option>' . "\n";
						echo '<option  value="30"' . ( ( $webinarday == '30' ) ? 'selected="selected"' : '' ) . '>30</option>' . "\n";
						echo '<option  value="31"' . ( ( $webinarday == '31' ) ? 'selected="selected"' : '' ) . '>31</option>' . "\n";
						echo '</select>';
						break;

					case 'yearselect':
						global $post;
						$post_id     = $post->ID;
						$webinaryear = get_post_meta( $post_id, 'wpwebinar_webinar_year', true );
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option  value="2011"' . ( ( $webinaryear == '2011' ) ? 'selected="selected"' : '' ) . '>2011</option>' . "\n";
						echo '<option  value="2012"' . ( ( $webinaryear == '2012' ) ? 'selected="selected"' : '' ) . '>2012</option>' . "\n";
						echo '<option  value="2013"' . ( ( $webinaryear == '2013' ) ? 'selected="selected"' : '' ) . '>2013</option>' . "\n";
						echo '<option  value="2014"' . ( ( $webinaryear == '2014' ) ? 'selected="selected"' : '' ) . '>2014</option>' . "\n";
						echo '<option  value="2015"' . ( ( $webinaryear == '2015' ) ? 'selected="selected"' : '' ) . '>2015</option>' . "\n";
						echo '<option  value="2016"' . ( ( $webinaryear == '2016' ) ? 'selected="selected"' : '' ) . '>2016</option>' . "\n";
						echo '<option  value="2017"' . ( ( $webinaryear == '2017' ) ? 'selected="selected"' : '' ) . '>2017</option>' . "\n";
						echo '<option  value="2018"' . ( ( $webinaryear == '2018' ) ? 'selected="selected"' : '' ) . '>2018</option>' . "\n";
						echo '<option  value="2019"' . ( ( $webinaryear == '2019' ) ? 'selected="selected"' : '' ) . '>2019</option>' . "\n";
						echo '<option  value="2020"' . ( ( $webinaryear == '2020' ) ? 'selected="selected"' : '' ) . '>2020</option>' . "\n";
						echo '<option  value="2021"' . ( ( $webinaryear == '2021' ) ? 'selected="selected"' : '' ) . '>2021</option>' . "\n";
						echo '</select>';
						break;

					case 'timezoneselect':
						global $post;
						$post_id         = $post->ID;
						$webinartimezone = get_post_meta( $post_id, 'wpwebinar_webinar_timezone', true );
						echo '<div id ="admintimebox" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<select name="', $field['id'], '" id="', $field['id'], '">';
						echo '<option value="Pacific/Midway"' . ( ( $webinartimezone == 'Pacific/Midway' ) ? 'selected="selected"' : '' ) . '>(GMT-11:00) Midway Island, Samoa</option>' . "\n";
						echo '<option value="America/Adak"' . ( ( $webinartimezone == 'America/Adak' ) ? 'selected="selected"' : '' ) . '>(GMT-10:00) Hawaii-Aleutian</option>' . "\n";
						echo '<option value="Etc/GMT+10"' . ( ( $webinartimezone == 'Etc/GMT+10' ) ? 'selected="selected"' : '' ) . '>(GMT-10:00) Hawaii</option>' . "\n";
						echo '<option value="Pacific/Marquesas"' . ( ( $webinartimezone == 'Pacific/Marquesas' ) ? 'selected="selected"' : '' ) . '>(GMT-09:30) Marquesas Islands</option>' . "\n";
						echo '<option value="Pacific/Gambier"' . ( ( $webinartimezone == 'Pacific/Gambier' ) ? 'selected="selected"' : '' ) . '>(GMT-09:00) Gambier Islands</option>' . "\n";
						echo '<option value="America/Anchorage"' . ( ( $webinartimezone == 'America/Anchorage' ) ? 'selected="selected"' : '' ) . '>(GMT-09:00) Alaska</option>' . "\n";
						echo '<option value="America/Ensenada"' . ( ( $webinartimezone == 'America/Ensenada' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Tijuana, Baja California</option>' . "\n";
						echo '<option value="Etc/GMT+8"' . ( ( $webinartimezone == 'Etc/GMT+8' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Pitcairn Islands</option>' . "\n";
						echo '<option value="America/Los_Angeles"' . ( ( $webinartimezone == 'America/Los_Angeles' ) ? 'selected="selected"' : '' ) . '>(GMT-08:00) Pacific Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Denver"' . ( ( $webinartimezone == 'America/Denver' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Mountain Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Chihuahua"' . ( ( $webinartimezone == 'America/Chihuahua' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>' . "\n";
						echo '<option value="America/Dawson_Creek"' . ( ( $webinartimezone == 'America/Dawson_Creek' ) ? 'selected="selected"' : '' ) . '>(GMT-07:00) Arizona</option>' . "\n";
						echo '<option value="America/Belize"' . ( ( $webinartimezone == 'America/Belize' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Saskatchewan, Central America</option>' . "\n";
						echo '<option value="America/Cancun"' . ( ( $webinartimezone == 'America/Cancun' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>' . "\n";
						echo '<option value="Chile/EasterIsland"' . ( ( $webinartimezone == 'Chile/EasterIsland' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Easter Island</option>' . "\n";
						echo '<option value="America/Chicago"' . ( ( $webinartimezone == 'America/Chicago' ) ? 'selected="selected"' : '' ) . '>(GMT-06:00) Central Time (US & Canada)</option>' . "\n";
						echo '<option value="America/New_York"' . ( ( $webinartimezone == 'America/New_York' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Eastern Time (US & Canada)</option>' . "\n";
						echo '<option value="America/Havana"' . ( ( $webinartimezone == 'America/Havana' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Cuba</option>' . "\n";
						echo '<option value="America/Bogota"' . ( ( $webinartimezone == 'America/Bogota' ) ? 'selected="selected"' : '' ) . '>(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>' . "\n";
						echo '<option value="America/Caracas"' . ( ( $webinartimezone == 'America/Caracas' ) ? 'selected="selected"' : '' ) . '>(GMT-04:30) Caracas</option>' . "\n";
						echo '<option value="America/Santiago"' . ( ( $webinartimezone == 'America/Santiago' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Santiago</option>' . "\n";
						echo '<option value="America/La_Paz"' . ( ( $webinartimezone == 'America/La_Paz' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) La Paz</option>' . "\n";
						echo '<option value="Atlantic/Stanley"' . ( ( $webinartimezone == 'Atlantic/Stanley' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Faukland Islands</option>' . "\n";
						echo '<option value="America/Campo_Grande"' . ( ( $webinartimezone == 'America/Campo_Grande' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Brazil</option>' . "\n";
						echo '<option value="America/Goose_Bay"' . ( ( $webinartimezone == 'America/Goose_Bay' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Atlantic Time (Goose Bay)</option>' . "\n";
						echo '<option value="America/Glace_Bay"' . ( ( $webinartimezone == 'America/Glace_Bay' ) ? 'selected="selected"' : '' ) . '>(GMT-04:00) Atlantic Time (Canada)</option>' . "\n";
						echo '<option value="America/St_Johns"' . ( ( $webinartimezone == 'America/St_Johns' ) ? 'selected="selected"' : '' ) . '>(GMT-03:30) Newfoundland</option>' . "\n";
						echo '<option value="America/Araguaina"' . ( ( $webinartimezone == 'America/Araguaina' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) UTC-3</option>' . "\n";
						echo '<option value="America/Montevideo"' . ( ( $webinartimezone == 'America/Montevideo' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Montevideo</option>' . "\n";
						echo '<option value="America/Miquelon"' . ( ( $webinartimezone == 'America/Miquelon' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Miquelon, St. Pierre</option>' . "\n";
						echo '<option value="America/Godthab"' . ( ( $webinartimezone == 'America/Godthab' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Greenland</option>' . "\n";
						echo '<option value="America/Argentina/Buenos_Aires"' . ( ( $webinartimezone == 'America/Argentina/Buenos_Aires' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Buenos Aires</option>' . "\n";
						echo '<option value="America/Sao_Paulo"' . ( ( $webinartimezone == 'America/Sao_Paulo' ) ? 'selected="selected"' : '' ) . '>(GMT-03:00) Brasilia</option>' . "\n";
						echo '<option value="America/Noronha"' . ( ( $webinartimezone == 'America/Noronha' ) ? 'selected="selected"' : '' ) . '>(GMT-02:00) Mid-Atlantic</option>' . "\n";
						echo '<option value="Atlantic/Cape_Verde"' . ( ( $webinartimezone == 'Atlantic/Cape_Verde' ) ? 'selected="selected"' : '' ) . '>(GMT-01:00) Cape Verde Is.</option>' . "\n";
						echo '<option value="Atlantic/Azores"' . ( ( $webinartimezone == 'Atlantic/Azores' ) ? 'selected="selected"' : '' ) . '>(GMT-01:00) Azores</option>' . "\n";
						echo '<option value="Europe/Belfast"' . ( ( $webinartimezone == 'Europe/Belfast' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Belfast</option>' . "\n";
						echo '<option value="Europe/Dublin"' . ( ( $webinartimezone == 'Europe/Dublin' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Dublin</option>' . "\n";
						echo '<option value="Europe/Lisbon"' . ( ( $webinartimezone == 'Europe/Lisbon' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : Lisbon</option>' . "\n";
						echo '<option value="Europe/London"' . ( ( $webinartimezone == 'Europe/London' ) ? 'selected="selected"' : '' ) . '>(GMT) Greenwich Mean Time : London</option>' . "\n";
						echo '<option value="Africa/Abidjan"' . ( ( $webinartimezone == 'Africa/Abidjan' ) ? 'selected="selected"' : '' ) . '>(GMT) Monrovia, Reykjavik</option>' . "\n";
						echo '<option value="Europe/Amsterdam"' . ( ( $webinartimezone == 'Europe/Amsterdam' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>' . "\n";
						echo '<option value="Europe/Belgrade"' . ( ( $webinartimezone == 'Europe/Belgrade' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>' . "\n";
						echo '<option value="Europe/Brussels"' . ( ( $webinartimezone == 'Europe/Brussels' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>' . "\n";
						echo '<option value="Africa/Algiers"' . ( ( $webinartimezone == 'Africa/Algiers' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) West Central Africa</option>' . "\n";
						echo '<option value="Africa/Windhoek"' . ( ( $webinartimezone == 'Africa/Windhoek' ) ? 'selected="selected"' : '' ) . '>(GMT+01:00) Windhoek</option>' . "\n";
						echo '<option value="Asia/Beirut"' . ( ( $webinartimezone == 'Asia/Beirut' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Beirut</option>' . "\n";
						echo '<option value="Africa/Cairo"' . ( ( $webinartimezone == 'Africa/Cairo' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Cairo</option>' . "\n";
						echo '<option value="Asia/Gaza"' . ( ( $webinartimezone == 'Asia/Gaza' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Gaza</option>' . "\n";
						echo '<option value="Africa/Blantyre"' . ( ( $webinartimezone == 'Africa/Blantyre' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Harare, Pretoria</option>' . "\n";
						echo '<option value="Asia/Jerusalem"' . ( ( $webinartimezone == 'Asia/Jerusalem' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Jerusalem</option>' . "\n";
						echo '<option value="Europe/Minsk"' . ( ( $webinartimezone == 'Europe/Minsk' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Minsk</option>' . "\n";
						echo '<option value="Asia/Damascus"' . ( ( $webinartimezone == 'Asia/Damascus' ) ? 'selected="selected"' : '' ) . '>(GMT+02:00) Syria</option>' . "\n";
						echo '<option value="Europe/Moscow"' . ( ( $webinartimezone == 'Europe/Moscow' ) ? 'selected="selected"' : '' ) . '>(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>' . "\n";
						echo '<option value="Africa/Addis_Ababa"' . ( ( $webinartimezone == 'Africa/Addis_Ababa' ) ? 'selected="selected"' : '' ) . '>(GMT+03:00) Nairobi</option>' . "\n";
						echo '<option value="Asia/Tehran"' . ( ( $webinartimezone == 'Asia/Tehran' ) ? 'selected="selected"' : '' ) . '>(GMT+03:30) Tehran</option>' . "\n";
						echo '<option value="Asia/Dubai"' . ( ( $webinartimezone == 'Asia/Dubai' ) ? 'selected="selected"' : '' ) . '>(GMT+04:00) Abu Dhabi, Muscat</option>' . "\n";
						echo '<option value="Asia/Yerevan"' . ( ( $webinartimezone == 'Asia/Yerevan' ) ? 'selected="selected"' : '' ) . '>(GMT+04:00) Yerevan</option>' . "\n";
						echo '<option value="Asia/Kabul"' . ( ( $webinartimezone == 'Asia/Kabul' ) ? 'selected="selected"' : '' ) . '>(GMT+04:30) Kabul</option>' . "\n";
						echo '<option value="Asia/Yekaterinburg"' . ( ( $webinartimezone == 'Asia/Yekaterinburg' ) ? 'selected="selected"' : '' ) . '>(GMT+05:00) Ekaterinburg</option>' . "\n";
						echo '<option value="Asia/Tashkent"' . ( ( $webinartimezone == 'Asia/Tashkent' ) ? 'selected="selected"' : '' ) . '>(GMT+05:00) Tashkent</option>' . "\n";
						echo '<option value="Asia/Kolkata"' . ( ( $webinartimezone == 'Asia/Kolkata' ) ? 'selected="selected"' : '' ) . '>(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>' . "\n";
						echo '<option value="Asia/Katmandu"' . ( ( $webinartimezone == 'Asia/Katmandu' ) ? 'selected="selected"' : '' ) . '>(GMT+05:45) Kathmandu</option>' . "\n";
						echo '<option value="Asia/Dhaka"' . ( ( $webinartimezone == 'Asia/Dhaka' ) ? 'selected="selected"' : '' ) . '>(GMT+06:00) Astana, Dhaka</option>' . "\n";
						echo '<option value="Asia/Novosibirsk"' . ( ( $webinartimezone == 'Asia/Novosibirsk' ) ? 'selected="selected"' : '' ) . '>(GMT+06:00) Novosibirsk</option>' . "\n";
						echo '<option value="Asia/Rangoon"' . ( ( $webinartimezone == 'Asia/Rangoon' ) ? 'selected="selected"' : '' ) . '>(GMT+06:30) Yangon (Rangoon)</option>' . "\n";
						echo '<option value="Asia/Bangkok"' . ( ( $webinartimezone == 'Asia/Bangkok' ) ? 'selected="selected"' : '' ) . '>(GMT+07:00) Bangkok, Hanoi, Jakarta</option>' . "\n";
						echo '<option value="Asia/Krasnoyarsk"' . ( ( $webinartimezone == 'Asia/Krasnoyarsk' ) ? 'selected="selected"' : '' ) . '>(GMT+07:00) Krasnoyarsk</option>' . "\n";
						echo '<option value="Asia/Hong_Kong"' . ( ( $webinartimezone == 'Asia/Hong_Kong' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>' . "\n";
						echo '<option value="Asia/Irkutsk"' . ( ( $webinartimezone == 'Asia/Irkutsk' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Irkutsk, Ulaan Bataar</option>' . "\n";
						echo '<option value="Australia/Perth"' . ( ( $webinartimezone == 'Australia/Perth' ) ? 'selected="selected"' : '' ) . '>(GMT+08:00) Perth</option>' . "\n";
						echo '<option value="Australia/Eucla"' . ( ( $webinartimezone == 'Australia/Eucla' ) ? 'selected="selected"' : '' ) . '>(GMT+08:45) Eucla</option>' . "\n";
						echo '<option value="Asia/Tokyo"' . ( ( $webinartimezone == 'Asia/Tokyo' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Osaka, Sapporo, Tokyo</option>' . "\n";
						echo '<option value="Asia/Seoul"' . ( ( $webinartimezone == 'Asia/Seoul' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Seoul</option>' . "\n";
						echo '<option value="Asia/Yakutsk"' . ( ( $webinartimezone == 'Asia/Yakutsk' ) ? 'selected="selected"' : '' ) . '>(GMT+09:00) Yakutsk</option>' . "\n";
						echo '<option value="Australia/Adelaide"' . ( ( $webinartimezone == 'Australia/Adelaide' ) ? 'selected="selected"' : '' ) . '>(GMT+09:30) Adelaide</option>' . "\n";
						echo '<option value="Australia/Darwin"' . ( ( $webinartimezone == 'Australia/Darwin' ) ? 'selected="selected"' : '' ) . '>(GMT+09:30) Darwin</option>' . "\n";
						echo '<option value="Australia/Brisbane"' . ( ( $webinartimezone == 'Australia/Brisbane' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Brisbane</option>' . "\n";
						echo '<option value="Australia/Hobart"' . ( ( $webinartimezone == 'Australia/Hobart' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Hobart</option>' . "\n";
						echo '<option value="Asia/Vladivostok"' . ( ( $webinartimezone == 'Asia/Vladivostok' ) ? 'selected="selected"' : '' ) . '>(GMT+10:00) Vladivostok</option>' . "\n";
						echo '<option value="Australia/Lord_Howe"' . ( ( $webinartimezone == 'Australia/Lord_Howe' ) ? 'selected="selected"' : '' ) . '>(GMT+10:30) Lord Howe Island</option>' . "\n";
						echo '<option value="Etc/GMT-11"' . ( ( $webinartimezone == 'Etc/GMT-11' ) ? 'selected="selected"' : '' ) . '>(GMT+11:00) Solomon Is., New Caledonia</option>' . "\n";
						echo '<option value="Asia/Magadan"' . ( ( $webinartimezone == 'Asia/Magadan' ) ? 'selected="selected"' : '' ) . '>(GMT+11:00) Magadan</option>' . "\n";
						echo '<option value="Pacific/Norfolk"' . ( ( $webinartimezone == 'Pacific/Norfolk' ) ? 'selected="selected"' : '' ) . '>(GMT+11:30) Norfolk Island</option>' . "\n";
						echo '<option value="Asia/Anadyr"' . ( ( $webinartimezone == 'Asia/Anadyr' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Anadyr, Kamchatka</option>' . "\n";
						echo '<option value="Pacific/Auckland"' . ( ( $webinartimezone == 'Pacific/Auckland' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Auckland, Wellington</option>' . "\n";
						echo '<option value="Etc/GMT-12"' . ( ( $webinartimezone == 'Etc/GMT-12' ) ? 'selected="selected"' : '' ) . '>(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>' . "\n";
						echo '<option value="Pacific/Chatham"' . ( ( $webinartimezone == 'Pacific/Chatham' ) ? 'selected="selected"' : '' ) . '>(GMT+12:45) Chatham Islands</option>' . "\n";
						echo '<option value="Pacific/Tongatapu"' . ( ( $webinartimezone == 'Pacific/Tongatapu' ) ? 'selected="selected"' : '' ) . '>(GMT+13:00) Nuku\'alofa</option>' . "\n";
						echo '<option value="Pacific/Kiritimati"' . ( ( $webinartimezone == 'Pacific/Kiritimati' ) ? 'selected="selected"' : '' ) . '>(GMT+14:00) Kiritimati</option>' . "\n";
						echo '</select>';
						echo '</div>';
						break;

					case 'shortselect':
						echo '<div id=\'wpwebinar_shortselect\' style=" width: 175px; border:1px solid #ccc; padding:10px 10px 0px 10px;border-radius:2px; margin-right: 15px; margin-bottom: 15px; float: left;">';
						echo '<label for="', $field['id'], '" style="border-bottom: 1px solid #ccc; padding-bottom: 10px;" >', $field['name'], '</label>';
						echo '<select style="margin-right: 15px; margin-top: 5px; float: left;" name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						echo '<p style="width: 100%;">', $field['desc'],'</p>';
						echo '</div>';
						break;

					case 'shortselect2':
						echo '<div id=\'wpwebinar_shortselect\' style=" width: 175px; border:1px solid #ccc; padding:10px 10px 0px 10px;border-radius:2px; margin-right: 15px; margin-bottom: 15px; float: left;">';
						echo '<label for="', $field['id'], '" style="border-bottom: 1px solid #ccc; padding-bottom: 10px;" >', $field['name'], '</label>';
						echo '<select style="margin-right: 15px; margin-top: 5px; float: left;" name="', $field['id'], '" id="', $field['id'], '">';
						foreach ( $field['options'] as $option ) {
							echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
						}
						echo '</select>';
						echo '<p style="width: 100%;">', $field['desc'],'</p>';
						echo '</div><div style="clear: both"></div>';
						break;

					case 'radio':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
						}
						break;

					case 'checkboxmedia':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						foreach ( $field['options'] as $option ) {
							echo '<input type="checkbox" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
						}
						break;

					case 'checkbox':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						break;

					case 'checkboxtheme':
						echo '<div id="theme_image_checkbox" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpwebinar_theme_basic\');uncheck_checkbox(\'wpwebinar_theme_modern\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxtheme1':
						echo '<div id="theme_image_checkbox1" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpwebinar_theme_yours\');uncheck_checkbox(\'wpwebinar_theme_modern\');"type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxtheme2':
						echo '<div id="theme_image_checkbox2" style="float: left; margin-right: 15px; margin-bottom: 15px; width: 150px; height: 110px">';
						echo '<input onclick="uncheck_checkbox(\'wpwebinar_theme_yours\');uncheck_checkbox(\'wpwebinar_theme_basic\');"type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
						echo '<label for="', $field['id'], '"></label></div>';
						break;

					case 'checkboxthemeoriginal':
						echo '<div id="theme_image_checkbox" style="float: left; width: 125px; height: 125px"><label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>';
						echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /></div>';
						break;

					// WEBINAR TIME CHECKBOX DAYS OF WEEK

					case 'checkboxtime':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxtimefirst':
						echo '<div id="recurring_box" style="display:none;border:1px solid #ccc;margin:10px 0px;padding:10px;border-radius:2px;">';
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxtimelast':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						echo '</div>';
						break;

					// MEDIA CHECKBOXES

					case 'checkboxvideo':
						echo '<!--Media Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div(\'video_box\',\'wpwebinar_show_video\');uncheck_checkbox(\'wpwebinar_show_audio\');uncheck_checkbox(\'wpwebinar_show_videobox\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxaudio':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div(\'audio_box\',\'wpwebinar_show_audio\');uncheck_checkbox(\'wpwebinar_show_video\');uncheck_checkbox(\'wpwebinar_show_videobox\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxvideobox':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div(\'videobox\',\'wpwebinar_show_videobox\');uncheck_checkbox(\'wpwebinar_show_video\');uncheck_checkbox(\'wpwebinar_show_audio\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// TIMEZONE CHECKBOXES

					case 'checkboxlocaltime':
						echo '<!--Timezone Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="hide_div4(\'admintimebox\',\'wpwebinar_admintime\');uncheck_checkbox(\'wpwebinar_admintime\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxadmintime':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div4(\'admintimebox\',\'wpwebinar_admintime\');uncheck_checkbox(\'wpwebinar_localtime\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// WEBINAR CHECKBOXES

					case 'checkboxwebinar':
						echo '<!--Webinar Selection-->' . "\n" . "\n";
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div3(\'onetime_box\',\'wpwebinar_onetime_webinar\');uncheck_checkbox(\'wpwebinar_recurring_webinar\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxwebinars':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div3(\'recurring_box\',\'wpwebinar_recurring_webinar\');uncheck_checkbox(\'wpwebinar_onetime_webinar\')" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// AUTORESPONDER CHECKBOXES

					case 'checkboxaweber':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'aweber_box\',\'wpwebinar_aweber\');uncheck_checkbox(\'wpwebinar_oneshoppingcart\');uncheck_checkbox(\'wpwebinar_getresponse\');uncheck_checkbox(\'wpwebinar_arp3\');uncheck_checkbox(\'wpwebinar_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkbox1shoppingcart':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'1shoppingcart_box\',\'wpwebinar_oneshoppingcart\');uncheck_checkbox(\'wpwebinar_aweber\');uncheck_checkbox(\'wpwebinar_getresponse\');uncheck_checkbox(\'wpwebinar_arp3\');uncheck_checkbox(\'wpwebinar_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxgetresponse':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'getresponse_box\',\'wpwebinar_getresponse\');uncheck_checkbox(\'wpwebinar_aweber\');uncheck_checkbox(\'wpwebinar_oneshoppingcart\');uncheck_checkbox(\'wpwebinar_arp3\');uncheck_checkbox(\'wpwebinar_other\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div>' . "\n";
						break;

					case 'checkboxarp3':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'arp3_box\',\'wpwebinar_arp3\');uncheck_checkbox(\'wpwebinar_aweber\');uncheck_checkbox(\'wpwebinar_getresponse\');uncheck_checkbox(\'wpwebinar_oneshoppingcart\');uncheck_checkbox(\'wpwebinar_other\');"name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					case 'checkboxother':
						echo '<div id="cbstuff"><input type="checkbox" class="cbalign" onclick="show_div2(\'other_box\',\'wpwebinar_other\');uncheck_checkbox(\'wpwebinar_aweber\');uncheck_checkbox(\'wpwebinar_getresponse\');uncheck_checkbox(\'wpwebinar_oneshoppingcart\');uncheck_checkbox(\'wpwebinar_arp3\');" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' /><label class="cblabel">', $field['name'], '</label></div><div style="clear: both;"></div>' . "\n";
						break;

					// BUTTON CHECKBOXES

					case 'checkboxorange':
						echo '<label for="', $field['id'], '">', $field['name'], '</label><div class="description">', $field['desc'],'</div>' . "\n";
						echo '<input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpwebinar_button_green\');uncheck_checkbox(\'wpwebinar_button_blue\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img  src="' . WP_PLUGIN_URL . '/wp-webinar/images/orangebutton.png"/>' . "\n";
						break;

					case 'checkboxgreen':
						echo '<div style="margin: 10px 0px"><input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpwebinar_button_orange\');uncheck_checkbox(\'wpwebinar_button_blue\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img src="' . WP_PLUGIN_URL . '/wp-webinar/images/greenbutton.png"/></div>' . "\n";
						break;

					case 'checkboxblue':
						echo '<input style="margin-right: 10px" onclick="uncheck_checkbox(\'wpwebinar_button_orange\');uncheck_checkbox(\'wpwebinar_button_green\');" type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />' . "\n";
						echo '<img  src="' . WP_PLUGIN_URL . '/wp-webinar/images/bluebutton.png"/>' . "\n";
						break;
				}
			}
		endif;
	}
	echo '</div>';
}

// *********************************************************************************** //
// SAVE METABOX AND SANITIZE INFORMATION
// *********************************************************************************** //

add_action( 'save_post', 'wpwebinar_save_data' );

// Save data from meta box

function wpwebinar_save_data( $post_id ) {
	global $meta_boxes_webinar, $post_id;

	if ( ! isset( $_REQUEST['wpwebinar_meta_box_nonce'] ) ) {
		return;
	}

	// verify nonce
	if ( ! wp_verify_nonce( $_POST['wpwebinar_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	// check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	// check permissions
	if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $meta_boxes_webinar as $meta_box_webinar ) {
		foreach ( $meta_box_webinar['fields'] as $field ) {
			$old = get_post_meta( $post_id, $field['id'], true );
			$new = $_POST[ $field['id'] ];

			if ( $new && $new != $old ) {
				update_post_meta( $post_id, $field['id'], $new );
			} elseif ( '' == $new && $old ) {
				delete_post_meta( $post_id, $field['id'], $old );
			}
		}
	}
}


// *********************************************************************************** //
// FOOTER SCRIPTS
// *********************************************************************************** //


add_action( 'admin_footer', 'wpwebinar_enqueue_scripts' );

function wpwebinar_enqueue_scripts() {
	global $post;
	global $post_id;

	$showvideo       = get_post_meta( $post_id, 'wpwebinar_show_video', true );
	$showaudio       = get_post_meta( $post_id, 'wpwebinar_show_audio', true );
	$showvideobox    = get_post_meta( $post_id, 'wpwebinar_show_videobox', true );
	$aweber          = get_post_meta( $post_id, 'wpwebinar_aweber', true );
	$oneshoppingcart = get_post_meta( $post_id, 'wpwebinar_oneshoppingcart', true );
	$arp3            = get_post_meta( $post_id, 'wpwebinar_arp3', true );
	$getresponse     = get_post_meta( $post_id, 'wpwebinar_getresponse', true );
	$other           = get_post_meta( $post_id, 'wpwebinar_other', true );
	$onetime         = get_post_meta( $post_id, 'wpwebinar_onetime_webinar', true );
	$recurring       = get_post_meta( $post_id, 'wpwebinar_recurring_webinar', true );

	if ( $post && $post->post_type && $post->post_type == 'wpwebinar' ) {

		if ( $showvideo == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('video_box').style.display='block';</script>";
		} elseif ( $showaudio == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('audio_box').style.display='block';</script>";
		} elseif ( $showvideobox == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('videobox').style.display='block';</script>";
		}
		if ( $aweber == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('aweber_box').style.display='block';</script>";
		} elseif ( $oneshoppingcart == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('1shoppingcart_box').style.display='block';</script>";
		} elseif ( $getresponse == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('getresponse_box').style.display='block';</script>";
		} elseif ( $arp3 == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('arp3_box').style.display='block';</script>";
		} elseif ( $other == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('other_box').style.display='block';</script>";
		}
		if ( $onetime == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('onetime_box').style.display='block';</script>"; } elseif ( $recurring == 'on' ) {
			echo "<script type='text/javascript'>document.getElementById('recurring_box').style.display='block';</script>"; }
			?>
	  

		<?php
		// for autoresponder service auto show checkbox content
		// check above selected checkbox to echo out javascript to show selected box.

		global $post;
		$post_id = $post->ID;

		?>
	  

	<script type='text/javascript'>
		function uncheck_checkbox(fieldname){
			var div = document.forms.post.elements[fieldname];
			div.checked=false;
		}

		function hide_div() {
			var div = new Array('video_box','audio_box','videobox');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}

		function hide_div2() {
			var div = new Array('aweber_box','1shoppingcart_box','getresponse_box','arp3_box','other_box');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}

		}
		function hide_div3() {
			var div = new Array('onetime_box','recurring_box');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}

		function hide_div4(){
			var div = new Array('admintimebox');
			for(var i in div){
				document.getElementById(div[i]).style.display='none';	
			}
		}
		function show_div(div,checkdiv){
			hide_div();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true) {
				document.getElementById(div).style.display='block';
			}
			else{
				document.getElementById(div).style.display='none';
			}
		}
		function show_div2(div,checkdiv) {
			hide_div2();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true){
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
		function show_div3(div,checkdiv) {
			hide_div3();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true){
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
		function show_div4(div,checkdiv) {
			hide_div4();
			var checkbox = document.forms.post.elements[checkdiv];
			var div = div;
			if(checkbox.checked==true) {
				document.getElementById(div).style.display='block';
			}
			else {
				document.getElementById(div).style.display='none';
			}
		}
	</script>
<?php }
}

?>
